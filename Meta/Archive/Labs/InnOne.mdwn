This lab is in a large converted storeroom in the front range of the
inn.  The hypocaust doesn't reach this far so the floor is packed
earth and it is to be anticipated that it will get quite cold during
the winter (Defective Heating).  It is currently being used as a
storeroom for unwanted lab equipment and very little effort is being
made to keep it in good working order as a lab (Defective Lighting,
Dirty, Undecorated, Cramped).  Notable amongst the various things 
stored here are:

* A large limestone sarcophagus, undecorated.  Inside it is the
  skeleton of a young woman.
* A statue of a woman.  (Further inspection will reveal that it is a
  replica of the Athena Parthenos and probably once stood in the
  courtyard)
* An ornate two-pan balance; with weights marked in grains, scruples,
  drams, and ounces.
* An ornately carved Norman baptismal font
* A large copper cauldron, covered in verdigris.

These are all Lesser Features.  In addition one of the supply chests
is regularly refilled by the Inn's *lares*; sometimes with strange and
wonderful stuff (Faerie Ingredients)

| Size |                    | GQ | Up | Safe | Warp | Heal | Æsth | Vis Ex | Pe | Co | In | Experiment |
|-----:|:-------------------|:--:|:--:|:----:|:----:|:----:|:----:|:------:|:--:|:--:|:--:|:----------:|
|   -1 | Defective Heating  | -1 | -1 |      |      |   -1 |   -1 |        |    |    |    |            |
|   -1 | Defective Lighting | -1 | -1 |   -1 |      |      |   -1 |        |    |    |    |            |
|    0 | Dirty              |    |    |      |      |   -1 |   -2 |        | +1 |    |    |            |
|   -1 | Undecorated        |    | -1 |      |      |      |   -1 |     -1 | -1 |    |    |            |
|   -1 | Cramped            | -1 | -1 |   -2 |      |      |   -1 |     +1 |    |    |    |            |
|   +1 | Sarcophagus        |    |    |      |      |      |   +1 |        | +1 |    |    |            |
|   +1 | Statue             |    |    |      |      |      |   +1 |        |    | +1 |    |            |
|   +1 | Balance            |    |    |      |      |      |   +1 |     +1 |    |    |    |            |
|   +1 | Font               |    |    |      |      |      |   +1 |        |    |    | +1 |            |
|   +1 | Cauldron           |    |    |      |      |      |   +1 |     +1 |    |    |    |            |
|    0 | Faerie Ingredients | +1 | -1 |      |  +1  |      |      |        |    |    |    |  +2        |
| &nbsp; ||||||||||||

|   +1 | **Totals**         | -2 | -5 |   -3 |  +1  |   -2 |   -1 |     +2 | +1 | +1 | +1 |  +2        |
| &nbsp; ||||||||||||
[Lab Details]

Warping Personality Trait: Secretive +1

*GM Note: this lab in particular is not expected to be usable without work!*
