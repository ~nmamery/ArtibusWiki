This lab is filled with miscellaneous equipment and tools, but the eye
is immediately drawn to a large altar in the centre of the room
(Greater Focus).  The altar is similar in style to the font in
[[the store|InnOne]], and is surmounted by the clearest crystal ball
you've ever seen (Lesser Feature).  On further inspection many of the
things littering the room are examples of magical casting tools or
aids from non- or pre-hermitic traditions (Specimens, also a Realia of
Quality 4 for Vim).  Sadly the lab is very disorganised (Disorganized)
and essentially undecorated (Undecorated).  The hypocaust system keeps
this room warm (Magical Superior Heating).

| Size |                          | GQ | Up | Safe | Warp | Heal | Æsth | Vi | Ig | Mu |
|-----:|:-------------------------|:--:|:--:|:----:|:----:|:----:|:----:|:--:|:--:|:--:|
|   +3 | Altar                    |    |    |      |      |      |   +2 | +3 |    |    |
|   -3 | Altar                    | -2 |    |      |      |      |      | +4 |    |    |
|   +1 | Crystal Ball             |    |    |      |      |      |   +1 | +1 |    |    |
|   +1 | Specimens                |    | +1 |      |      |      |   +1 | +1 |    |    |
|    0 | Disorganized             |    |    |   -1 |      |      |   -1 |    |    | +1 |
|   -1 | Undecorated              |    | -1 |      |      |      |   -1 | -1 |    |    |
|    0 | Magical Superior Heating |    |    |      |      |   +1 |   +1 |    | +1 |    |
| &nbsp; |||||||||

|   +1 | **Totals**               | -2 | +1 |   -1 |      |   +1 |   +3 | +8 | +1 | +1 |
| &nbsp; |||||||||
