This is a page to talk about character ideas!

[[!toc ]]

# Spare Ideas

####Country Squire

* This might be better suited to someone who wasn't planning to be around very often, so I am offering it to anyone who fancies it.
    * A Herbam and Animal specialist, this mage spends much of his time in the Esk Valley holdings
    * A member of the minor nobility, perhaps a second son
    * Larger than life, friendly, gentle-Gifted. 

# Senji

 I have three Mage ideas; they're all going to depend some what on
whether people are going to be happy running certain bits of
background/plotty stuff.

#### Scottish student harmonist otter Bjornaer Mystery Cultist
* Will depend on someone being happy to run mystery cult plots in
downtime (which are likely to be fundamentally solo type stuff)
    * Zavier: I'd happily run mystery cult stuff
        * Shadow: Likewise.
* Also someone to take control of Loch Legean Tribunal
    * Shadow: I'd be happy to do this.

#### Graeco-Roman Faerie-blooded Merinita

* Would require someone to basically handle all the Faeries (there's
nothing more ridiculous than a Merinita who can't go on Merinita
plots!)
    * Zavier: I'd certainly be amused to be the Storyteller of Faerie
      though i warn you I'd be taking the courts seriously
        * Yay Faeries.  Etc.  
* Might require some mystery cult stuff too; but much less than the
above.


#### Transylvanian Tremere

* Would require someone to run basically all the rest of House
Tremere, including the Blackthorn covenant in Stonehenge.
    * Zavier: am i allowed to recanonise vampires if i did this?
        * House Tremere aren't Vampires :-p.
        * Well, they aren't magically created vampires as a result of
          some kind of misguided ritual spell.  The occasional one or
          two might have mythic/faerie vampire-type blood (although in
          ArM5 they appear to be quite wary about that too).
            * Zavier: Sniffs, certainly that is what everyone believes. No vampires exist in House Tremere and they will challenge anyone to certamen who dares to suggest otherwise. Therefore by the code it is settled, there are no vampires in House Tremere.


I don't have companion ideas, but depending on what people are happy
to run plots for I might well recycle parts of unused ideas from
above.

# Zavier

These are my current thoughts. Happy to change if people dislike them. 

#### Mage - Byzantine Ex Miscellanea Orbus Verditius
* illuminator, bookbinder and doctor in artibus. 

  Applying for a teaching position. 

  Would probably need some solo stories around his relationship with House Verditius and liable to try to take us towards collecting the most magnificent library in the world. 

  Not likely to leave the covenant itself much. 
  
  Plan would be to make him a rego corpus specialist who makes illuminated pictures that take you to where (or who) is depicted and books of quality that he will sell or give to the covenant so long as worthy books are copied into them. 

#### Companion - Edward Hardrada - 
* child of Harold and Edith Swanneck (who may possibly have been an
infernal plant who encouraged her husband to make stupid tactical
errors). 

  Has spent time in an infernal regio where time moves differently and
  thus is not 160 odd years old but has been mentored in demons. 
  
  Arguably heir to the throne of England. There is an infernal
  prophecy that "He will die King of England, Scots and divers other
  lands but not until he brings them to wrack and ruin." Since he does
  not want to die or to bring is country to ruin, he has spent his
  life ever since he found this out avoiding becoming King of
  England. 
  
  To be fair, very few people do become a King and even fewer of
  England. But he doesn't want to take chances so stopped being a
  mercenary warrior and wants to be the college Porter instead. He
  might settle for being the Turb Captain but wants a more innocuous
  title. Still few universities have quite so impressive a figure
  guarding their doors.  
    - The Death Prophecy there is quite rigidly, umm, rigid.  I don't
      have the rulebook with me but they're supposed to be
      flexible/misinterpretable (e.g. "fear the boar" would cover men
      with pigs on the heraldry and bad bacon).  -- Senji
    - Fair point. Happy to water it down. How about "he shall die King of a Ruined Land."? He might assume England but many other options might apply including being crowned as a joke. Zavier

*I see a general problem here that neither of these characters seem
 very inclined to step outside their hallowed walls for adventures...*
    - The mage maybe not (to some extent a problem with any academic character I might make I suspect) but he would leave to find interesting arcane connections or to retrieve any artifact of byzantium he hears about for instance. Or for that matter almost any book! The consor would be more likely to as he is a) liable to meddling by the infernal and b) hates it with a passion for what it has done. So even if he would like to cower in the safe walls, I'd see him as someone who would instantly leave if he heard some things. Plus of course if he is the Turb Captain he would leave whenever he was commanded to by the mages or if a number of the Turb were sent out. Zav


----
Note to Zavier:

That's a [[definition list|https://github.com/fletcher/MultiMarkdown/wiki/MultiMarkdown-Syntax-Guide#definition-lists]].
It doesn't have levels like that :-p

I've reformatted it to a normal kind of listy list.

Zavier - oops

----

#Shadow

###Mage

####Young Guernicus Scholar

* A scholar as well as a mage, this young man is studying mundane law as well as magical
* *While no one would dream of interfering with the mundanes, and while magical law would clearly take precedence, sometimes it's useful to know what the nobility of the land believe to be the law...*
* Intellego, Mentem, probably Vim, possibly Rego, Imaginem
* Gentle Gift

###Companion

####The Covenant Cat

* Initial concept was to have a companion who was a cat who was able to shapeshift into human form
* A member of the notable line of black magical cats (Realms of Magic)
* Curious, mercurial, protective, charming.

---- 

#Ilanin

###Mage(s)

#### Mercere (works equally well as a Verditius, but we've already got one of them so...) "Engineer"

* Obsessed with making a better X. It doesn't really matter what X is, he just likes tinkering with things to make them better. 
* Plays Q to non-gifted Redcaps. And anyone else who will test his devices out, including himself. 
* Obviously this means a Creo/Muto specialist. Probably a generalist in terms of forms; there will probably be some Corpus involved given that as a Creo expert it would be daft not to...
* Very curious/neophiliac - chases new experiences (and things to tinker with!). 
    * My cat would adore this character... S
