[[Planets' Blessing|PlanetsBlessing]] (Non Hermetic)
----------------------------------------------------

Creo Vim 45

**R**: Personal, **D**: Momentary, **T**: Ind, Ritual

You gain a bonus to your casting total
for a spell you cast immediately following
this ritual, equal to at most 30. The spell is associated with seven different
planets, each associated with a personality trait and Form (see [[table|Setting/Locations/Stonehenge/ScholaPythagoranis/Library/S.ArL.002]])

Your score in this Personality trait (or
a closely related trait), multiplied by ten,
determines the maximum bonus granted
by this ritual. The boosted spell must only
affect the associated Form and no others. If it
includes other requisites, there is no
bonus. This spell may be used
with formulaic or spontaneous magic, and
may even be used with rituals provided
that you begin casting the boosted spell
immediately after finishing the Blessing.
(Non-Hermetic)

A copy of this is in the library. ([[Setting/Locations/Stonehenge/ScholaPythagoranis/Library/LT.CrVi045.001]])
