[[!template id=character name="Emyn" house=" " job=" " rank=" " appage=  born=1100/25/12 updated=1237/06/21 covenant="Schola Pythagoranis" parens=" " origin="Cambridge" ethnicity="Unicorn" religion=" " age=135 ]]
## Emyn

----
[[Main|Emyn]] / [[Powers|Emyn/Powers]] / [[Skills|Emyn/Skills]] / [[Stats|Emyn/Stats]] / [[Additional Detail|Emyn/AdditionalDetail]]

----
[[Powers|Emyn/Powers#Powers]]

----
<a id="Powers" />
### Powers
Spell Name|Art 1|Art 2|Requisites|Level|Type|Range|Duration|Target|Description|MXP|M|Mastered Abilities|Spell Use|Notes
:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--:|:--:|:--|:--|:--
[[Teleport|/Meta/Spells/Teleport]]|Rego|Corpus| |15|Lesser|Touch|Momentary|Individual|Teleport up to 5 paces| |0| | |Restricted Power: celibate targets. Might cost 3 
[[Unseen Porter|/Meta/Spells/UnseenPorter]]|Rego|Terram| |10|Lesser|Voice|Concentration|Individual|See p156 main book| |0| | |Might cost 2
[[Detect Innocence|/Meta/Spells/DetectInnocence]]|Intellego|Mentem| |30|Personal|Personal|Momentary|Smell|Can scent whether the target is innocent| |0| | |Might cost 3
[[Detect the Gift|/Meta/Spells/DetecttheGift]]|Intellego|Vim| |25|Personal|Personal|Concentration|Smell|Can scent whether the target is Gifted| |0| | |Might cost 3
[[Defending the Herd|/Meta/Spells/DefendingtheHerd]]|Intellego|Vim| |15|Personal|Personal|Sun|Smell|Allows you to smell across regio boundaries| |0| | |Might cost 2
[[Illumination|/Meta/Spells/Illumination]]|Creo|Ignem| |5|Personal|Personal|Concentration|Part|Makes your horn glow as if with torchlight| |0| | |Might cost 1
[[Cleanse|/Meta/Spells/Cleanse]]|Creo|Corpus| |40|Ritual|Touch|Momentary|Individual|Heals the target of all wounds caused by poison and disease| |0|Might cost reduction x2| |Might cost 6
[[Shapeshift|/Meta/Spells/Shapeshift]]|Muto|Animal|Corpus|28|Bond|Touch|Concentration|Individual|Assume human form| |0| | |Might cost 0; Bond power. Bond maintains concentration. Six uses per day.
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
&nbsp;||||||||||||||
