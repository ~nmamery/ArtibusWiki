[[!template id=book name="Bologna & Cambridge Vol. 1 – The Mathematics of Beauty" author="Alexios Kaloethes & Georgios Herodontes" lang="Classical Greek" subject="Artes Liberales (Mathematics)" quality="9" scribe="Yes" binder="Yes" illumination="Yes"]]

A series of letters between two academics, Dr Kaloethes who comes from a mathematical standpoint and Dr Herodontes, a theologian and astrologer, on the topic of art and how it is reflected in the natural world. First published in Autumn 1218. (Alexios’ Comm 3 + Georgios’ Comm 3 + 3 from Skilled Artisans = 9)

