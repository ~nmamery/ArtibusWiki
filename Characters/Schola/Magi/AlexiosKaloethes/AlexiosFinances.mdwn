[[!template id=character name="Alexios Constantine Commenes Kaloethes" house="Ex Miscellanea" job="Doctor of Philosophy" rank="Junior Magus" appage=31 born=1165/03/21 updated=1220/06/01 covenant="Schola Pythagoranis" parens="Georgios of Bologna" origin="Constantinople" ethnicity="Roman-Greek" religion="Orthodox" age=55 image=""" [[!img Alexios.png size="200x" alt="Alexios.png"]] """]]
## Alexios Constantine Commenes Kaloethes

----
[[Main|AlexiosKaloethes]] / [[Magic|AlexiosKaloethes/Magic]] / [[Skills|AlexiosKaloethes/Skills]] / [[Stats|AlexiosKaloethes/Stats]] / [[Additional Detail|AlexiosKaloethes/AdditionalDetail]]

----

##### Personal Finances

Current Balance:

* Year: 1240
* vTR: 83
* vF: 25
* Silver: 14 


One off gains:

* Star Ruby (value c. 14 pounds, ex-Vis container, from Anteverpium)

Regular Income:

* Alexios has 6 pawns of Rego (vR) vis/year. He gets 3 pawns on each Equinox 
* Alexios lectures every year in Spring (2 pawns of vF) from 1231 onwards he teaches an apprentice (2 pawns of vF) 
* Alexios can sell up to 2vT for 22 pounds of silver per year each (1 pawn until 1225). He can sell additional vT pawns to the Redcaps for 20 pounds of silver. 

Regular Costs: 

* Dependent’s Living Costs: 43 pounds (1220-1222: 30 pounds, 1223 – 1225: 34 pounds. Includes cost of Guiscard the Norman; see [[Hiring Hall|/Schola/Covenfolk/HiringRoom/]])
* Lab Upkeep: 9 pounds (1227 – 1235: 1.5 pounds, 1235 – 1239: 3.5 pounds) 
* Lab Servant:  6 pounds (1225 onwards: Peter Ericsson. In 1225, Peter is 16 and has +3 intelligence)
* From 1228, Alexios spends 9 pounds a year on a Forge Companion, Rose.
* Silver Tips: 2 pounds (1224 – 1226: 1 pound, 1227 – 1236: 1.5 pounds.)
* Redcap Tips: 1vf
