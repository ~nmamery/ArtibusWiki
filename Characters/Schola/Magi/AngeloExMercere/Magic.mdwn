[[!template id=character name="Angelo ex Mercere" house="Mercere" job="Magus" rank="Junior Magus" appage=35 born=1190/08/30 updated=1243/03/21 covenant="Schola Pythagoranis" parens="Pietro ex Mercere" origin="Harco, Piedmont" ethnicity="Italian" religion="Catholic, mostly" age=52 ]]
## Angelo ex Mercere

----
[[Main|AngeloExMercere]] / [[Magic|AngeloExMercere/Magic]] / [[Skills|AngeloExMercere/Skills]] / [[Stats|AngeloExMercere/Stats]] / [[Additional Detail|AngeloExMercere/AdditionalDetail]]

----
[[Casting Table|AngeloExMercere/Magic#Casting]] | [[Casting Table Notes|AngeloExMercere/Magic#CTNotes]] | [[Notes|AngeloExMercere/Magic#Notes]] | [[Techniques|AngeloExMercere/Magic#Techniques]] | [[Forms|AngeloExMercere/Magic#Forms]] | [[Spells|AngeloExMercere/Magic#Spells]]

----
[[!template id=divbox content="""
<a id=Casting" />
### Casting Table
Technique|Form|T+F|CS|LT|FC|SC|SS|C(NS)|C(S)|R|P|MR
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Creo|Animal|27|28|41|28+die|6|14+(dStress/2)|7.2|17+(dStress/2)|34+dStress|20|22
 |Aquam|23|24|37|24+die|5|12+(dStress/2)|6.2|15+(dStress/2)|30+dStress|20|21
 |Auram|24|25|38|25+die|5|12.5+(dStress/2)|6.2|15.5+(dStress/2)|31+dStress|20|22
 |Corpus|33|34|47|34+die|7|17+(dStress/2)|8.2|20+(dStress/2)|40+dStress|25|28
 |Herbam|25|26|39|26+die|5|13+(dStress/2)|6.2|16+(dStress/2)|32+dStress|20|22
 |Ignem|27|28|41|28+die|6|14+(dStress/2)|7.2|17+(dStress/2)|34+dStress|20|22
 |Imaginem|24|25|38|25+die|5|12.5+(dStress/2)|6.2|15.5+(dStress/2)|31+dStress|20|22
 |Mentem|23|24|37|24+die|5|12+(dStress/2)|6.2|15+(dStress/2)|30+dStress|20|21
 |Terram|24|25|38|25+die|5|12.5+(dStress/2)|6.2|15.5+(dStress/2)|31+dStress|20|22
 |Vim|24|25|38|25+die|5|12.5+(dStress/2)|6.2|15.5+(dStress/2)|31+dStress|20|22
&nbsp;||||||||||||

Intellego|Animal|17|18|31|18+die|4|9+(dStress/2)|5.2|12+(dStress/2)|24+dStress|20|22
 |Aquam|13|14|27|14+die|3|7+(dStress/2)|4.2|10+(dStress/2)|20+dStress|20|21
 |Auram|14|15|28|15+die|3|7.5+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|20|22
 |Corpus|23|24|37|24+die|5|12+(dStress/2)|6.2|15+(dStress/2)|30+dStress|25|28
 |Herbam|15|16|29|16+die|3|8+(dStress/2)|4.2|11+(dStress/2)|22+dStress|20|22
 |Ignem|17|18|31|18+die|4|9+(dStress/2)|5.2|12+(dStress/2)|24+dStress|20|22
 |Imaginem|14|15|28|15+die|3|7.5+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|20|22
 |Mentem|13|14|27|14+die|3|7+(dStress/2)|4.2|10+(dStress/2)|20+dStress|20|21
 |Terram|14|15|28|15+die|3|7.5+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|20|22
 |Vim|14|15|28|15+die|3|7.5+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|20|22
&nbsp;||||||||||||

Muto|Animal|24|25|38|25+die|5|12.5+(dStress/2)|6.2|15.5+(dStress/2)|31+dStress|20|22
 |Aquam|20|21|34|21+die|4|10.5+(dStress/2)|5.2|13.5+(dStress/2)|27+dStress|20|21
 |Auram|21|22|35|22+die|4|11+(dStress/2)|5.2|14+(dStress/2)|28+dStress|20|22
 |Corpus|30|31|44|31+die|6|15.5+(dStress/2)|7.2|18.5+(dStress/2)|37+dStress|25|28
 |Herbam|22|23|36|23+die|5|11.5+(dStress/2)|6.2|14.5+(dStress/2)|29+dStress|20|22
 |Ignem|24|25|38|25+die|5|12.5+(dStress/2)|6.2|15.5+(dStress/2)|31+dStress|20|22
 |Imaginem|21|22|35|22+die|4|11+(dStress/2)|5.2|14+(dStress/2)|28+dStress|20|22
 |Mentem|20|21|34|21+die|4|10.5+(dStress/2)|5.2|13.5+(dStress/2)|27+dStress|20|21
 |Terram|21|22|35|22+die|4|11+(dStress/2)|5.2|14+(dStress/2)|28+dStress|20|22
 |Vim|21|22|35|22+die|4|11+(dStress/2)|5.2|14+(dStress/2)|28+dStress|20|22
&nbsp;||||||||||||

Perdo|Animal|14|15|28|15+die|3|7.5+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|20|22
 |Aquam|10|11|24|11+die|2|5.5+(dStress/2)|3.2|8.5+(dStress/2)|17+dStress|20|21
 |Auram|11|12|25|12+die|2|6+(dStress/2)|3.2|9+(dStress/2)|18+dStress|20|22
 |Corpus|20|21|34|21+die|4|10.5+(dStress/2)|5.2|13.5+(dStress/2)|27+dStress|25|28
 |Herbam|12|13|26|13+die|3|6.5+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|20|22
 |Ignem|14|15|28|15+die|3|7.5+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|20|22
 |Imaginem|11|12|25|12+die|2|6+(dStress/2)|3.2|9+(dStress/2)|18+dStress|20|22
 |Mentem|10|11|24|11+die|2|5.5+(dStress/2)|3.2|8.5+(dStress/2)|17+dStress|20|21
 |Terram|11|12|25|12+die|2|6+(dStress/2)|3.2|9+(dStress/2)|18+dStress|20|22
 |Vim|11|12|25|12+die|2|6+(dStress/2)|3.2|9+(dStress/2)|18+dStress|20|22
&nbsp;||||||||||||

Rego|Animal|15|16|29|16+die|3|8+(dStress/2)|4.2|11+(dStress/2)|22+dStress|20|22
 |Aquam|11|12|25|12+die|2|6+(dStress/2)|3.2|9+(dStress/2)|18+dStress|20|21
 |Auram|12|13|26|13+die|3|6.5+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|20|22
 |Corpus|21|22|35|22+die|4|11+(dStress/2)|5.2|14+(dStress/2)|28+dStress|25|28
 |Herbam|13|14|27|14+die|3|7+(dStress/2)|4.2|10+(dStress/2)|20+dStress|20|22
 |Ignem|15|16|29|16+die|3|8+(dStress/2)|4.2|11+(dStress/2)|22+dStress|20|22
 |Imaginem|12|13|26|13+die|3|6.5+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|20|22
 |Mentem|11|12|25|12+die|2|6+(dStress/2)|3.2|9+(dStress/2)|18+dStress|20|21
 |Terram|12|13|26|13+die|3|6.5+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|20|22
 |Vim|12|13|26|13+die|3|6.5+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|20|22
"""]]
[[!template id=divbox content="""
<a id="CTNotes" />
### Casting Table Notes
Abbreviation | Full | Explanation
:--|:--|:--
T+F | Technique plus Form | 
CS | Casting Score | Base magical power without modifiers
FC | Formulaic Casting | Highest level of non-ritual spell castable (max level 45)
LT | Lab Total | Highest level of spell learnable
SC | Spontaneous Casting | Highest level of spell castable spontaneously without fatigue
SS | Stressed Spontaneous  | Highest level of spell castable spontaneously by taking fatigue
C (NS) | Ceremonial Casting | Take 15 minutes per magnitude to boost spontaneous casting
C (S) | Ceremonial Casting | Take 15 minutes per magnitude to boost stressed spontaneous casting
R | Ritual Casting | Ritual spells take longer but can be higher level than formulaic
P | Parma Magica | 
MR | Magic Resistance | 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Notes" />
### Notes
* If a form or technique is Deficient, halve all casting and lab totals that use that art after calculating as above. This includes halving the dice rolls.
* If you have Flawed Parma Magica, your Parma Magica is halved for the form in which it is flawed
* If you have incompatible arts, delete the relevant row.
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Techniques" />
### Techniques
:--|:--
Creo|15
Intellego|8
Muto|15
Perdo|5
Rego|6
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Forms" />
### Forms
:--|:--
Animal|9
Aquam|5
Auram|6
Corpus|15
Herbam|7
&nbsp;|
"""]]
[[!template id=divbox content="""
### Forms
:--|:--
Ignem|9
Imaginem|6
Mentem|5
Terram|6
Vim|6
&nbsp;|
"""]]
<br style="clear:left;" />
<a id="Spells" />
### Spells
Spell Name|Art 1|Art 2|Requisites|Level|Type|Range|Duration|Target|Description|MXP|M|Mastered Abilities|Spell Use|Sigil Manifestation
:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--:|:--:|:--|:--|:--
[[Doublet of Impenetrable Silk|/Meta/Spells/DoubletofImpenetrableSilk]]|Muto|Animal| |15|Formulaic|Touch|Sun|Individual|Enchants clothing to give +3 Soak|0|0| |Defensive|Apparently armoured panelling appears on the clothing 
[[The Chirugrgeon's Healing Touch|/Meta/Spells/TheChirugrgeonsHealingTouch]]|Creo|Corpus| |20|Ritual|Touch|Momentary|Individual|Heals a light wound|0|0| |Healing|Magical "stitches" appear in the healed wounds, then fade
[[Purification of the Festering Wounds|/Meta/Spells/PurificationoftheFesteringWounds]]|Creo|Corpus| |20|Tamed|Touch|Moon|Individual|Gives +9 to recovery rolls|0|0| |Healing|The healing body smells sweet, as if it had been perfumed
[[Disguise of the New Visage|/Meta/Spells/DisguiseoftheNewVisage]]|Muto|Corpus| |15|Formulaic|Touch|Sun|Part|Change target's facial features|0|0| |Disguise|The new features are always well-groomed
[[Preternatural Growth and Shrinking|/Meta/Spells/PreternaturalGrowthandShrinking]]|Muto|Corpus| |15|Tamed|Touch|Sun|Individual|Change target size by +1 or up to -2|0|0| |Utility|The new size is always exactly the same ratio from the old size
[[Pilum of Fire|/Meta/Spells/PilumofFire]]|Creo|Ignem| |20|Tamed|Voice|Momentary|Individual|Throws a firebolt doing +15 damage|0|0| |Offensive|The pilum appears as a fiery version of a Roman pilum, not a shapeless bolt
[[Charge of the Angry Winds|/Meta/Spells/ChargeoftheAngryWinds]]|Creo|Auram| |15|Tamed|Voice|Concentration|Individual|Strong winds issue up to 10 paces from caster, blocks missile fire, Dex + Size stress at 9+ not to fall over, Strength + Size at 15+ to move against (Dex + Size 12+ not to fall over if this fails)|0|0| |Mostly Defensive|The winds appear close to Angelo and blow in precise straight lines
[[Repair Cracks and Tears|/Meta/Spells/RepairCracksandTears]]|Creo|Animal| |20|Ritual|Touch|Momentary|Individual|Repairs a book|0|0| |Library maintenance|Magical "stitches" appear in the healed cracks, then fade
[[Lungs of the Fish|/Meta/Spells/LungsoftheFish]]|Muto|Aquam|Auram|20|Formulaic|Touch|Sun|Part|Allows breathing underwater|0|0| |Utility|The voice of the spell's target sounds artificial 
[[Muninn's Satchel|/Meta/Spells/MuninnsSatchel]]|Muto|Animal| |15|Tamed|Touch|Moon|Individual|Turns a message into a raven's feather|0|0| |Messages|The feather is unnaturally shiny
[[Cloak of Black Feathers|/Meta/Spells/CloakofBlackFeathers]]|Muto|Corpus|Animal|30|Formulaic|Personal|Sun|Individual|Turns caster into a raven|0|0| |Transport|The raven is completely black, rather than naturally raven coloured
[[Magician's Mortar|/Meta/Spells/MagiciansMortar]]|Creo|Terram| |20|Ritual, Tamed|Touch|Momentary|Structure +1|Repairs a stone or brick building provided it's still standing|0|0| |Engineering|The repair work shows a distinct "join" which fades over time
[[Incantation of the Body Made Whole|/Meta/Spells/IncantationoftheBodyMadeWhole]]|Creo|Corpus| |40|Ritual, Tamed|Touch|Momentary|Individual|Heals all wounds to a human body when the ritual finishes|0|0| |Healing|Magical "stitches" appear in the healed wounds, then fade
[[Gentle Touch of the Purified Body|/Meta/Spells/GentleTouchofthePurifiedBody]]|Creo|Corpus| |20|Ritual|Touch|Momentary|Individual|Heals a light wound caused by poison or disease|0|0| |Healing|The damaged parts of the body smell sweet as they heal
[[The Bountiful Flock|/Meta/Spells/TheBountifulFlock]]|Creo|Animal| |30|Ritual, Tamed|Touch|Year|Boundary|Must be cast on the day after the vernal equinox. All animals in the target structure will be healthy, fertile, grow well, and be tastier than they would otherwise be. The animals also gain a +1 to their "animal" trait.| |0| |Economic|The colour of the animal's hair or hide is an exaggeratedly pure tone of its normal colour
[[To Make a Stand|/Meta/Spells/ToMakeaStand]]|Creo|Herbam| |20|Tamed|Touch|Moon|Individual|Bring a plant to maturity over the duration of the spell| |0| |Library, sort of|The plant's leaves grow in a neatly arranged geometric pattern
[[Demeter's Aegis|/Meta/Spells/DemetersAegis]]|Creo|Herbam| |10|Tamed|Touch|Sun|Individual|Makes a wooden shield. Finesse roll required to determine how good the construction is| |0| |Defensive|The Shield has upon it a design of a Unicorn Rampant
[[Raise the Spirits|/Meta/Spells/RaisetheSpirits]]|Creo|Aquam| |25|Tamed|Touch|Moon|Group|Ferment alcoholic drink over the duration. The drink turns several bright colours during the fermentation process and bubbles and fizzes; if the spell is cast on a liquid in a sealed container the container will burst.| |0| |Economic|Exaggerated into the spell description
[[To Make a Copse|/Meta/Spells/ToMakeaCopse]]|Creo|Herbam| |30|Tamed|Touch|Sun|Individual|Bring a plant to maturity over the duration of the spell| |0| |Research|The plant's leaves grow in a neatly arranged geometric pattern
[[To Pass the Seasoning|/Meta/Spells/ToPasstheSeasoning]]|Creo|Herbam| |30|Tamed|Voice|Moon|Group|Season wood over the duration of the spell. As a side effect, the wood is also bleached. | |0| |Economic|The grains, knots and lines of the wood fade during the seasoning
[[Years into Hours|/Meta/Spells/YearsintoHours]]|Creo|Animal| |35|Tamed|Touch|Sun|Individual|Causes the target animal to reach full maturity over the course of two hours| |0| |Research|The colour of the animal's hair shifts several tones during the aging
[[Charge of the Furious Winds|/Meta/Spells/ChargeoftheFuriousWinds]]|Creo|Auram| |25|Tamed|Voice|Concentration|Individual|Hurricane force winds issue up to 10 paces from caster, blocks missile fire, Dex + Size stress at 15+ not to fall over, Strength + Size at 18+ to move against (Dex + Size 18+ not to fall over if this fails)| |0| |Mostly Defensive|The winds appear close to Angelo and blow in precise straight lines
[[The Bountiful Feast|/Meta/Spells/TheBountifulFeast]]|Creo|Herbam| |35|Ritual, Tamed|Touch|Year|Boundary|Crops in the target area grow strongly/healthily for the duration. Must be cast on the day after the winter solstice.| |0| |Economic|The colours of the crops that grow are especially bright and vivid
[[Ball of Abysmal Flame|/Meta/Spells/BallofAbysmalFlame]]|Creo|Ignem| |35|Tamed|Voice|Momentary|Individual|Throws a fireball doing +30 damage| |0| |Offensive| 
[[Blade of the Virulent Flame|/Meta/Spells/BladeoftheVirulentFlame]]|Creo|Ignem| |15|Tamed|Touch|Diameter|Individual|Sets a metal weapon aflame; doubles damage or adds +5, whichever is greater, also can set stuff on fire.| |0| |Support| 
[[Flash of the Scarlet Flames|/Meta/Spells/FlashoftheScarletFlames]]|Creo|Ignem| |15|Formulaic|Voice|Momentary|Individual|Brilliant flash of blinding light; target makes Stm roll at 9+ not to be blinded, same roll once per minute to recover. | |0| |Non-Lethal | 
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
&nbsp;||||||||||||||
