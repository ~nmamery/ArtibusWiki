[[!template id=character name="Tanis" house="Merinita" job=" " rank="Junior Maga" appage=24 born=1187/05/12 updated=1244/01/31 covenant="Schola Pythagoranis" parens="Elly" origin="The Crimea" ethnicity="Graeco-Russian" religion="Orthodox" age=56 image=""" [[!img Tanis.jpg size="200x" alt="Tanis.jpg"]] """]]
## Tanis

----
[[Main|TanisKatsakisTanithou]] / [[Magic|TanisKatsakisTanithou/Magic]] / [[Skills|TanisKatsakisTanithou/Skills]] / [[Stats|TanisKatsakisTanithou/Stats]] / [[Additional Detail|TanisKatsakisTanithou/AdditionalDetail]]

----
[[Casting Table|TanisKatsakisTanithou/Magic#Casting]] | [[Casting Table Notes|TanisKatsakisTanithou/Magic#CTNotes]] | [[Notes|TanisKatsakisTanithou/Magic#Notes]] | [[Techniques|TanisKatsakisTanithou/Magic#Techniques]] | [[Forms|TanisKatsakisTanithou/Magic#Forms]] | [[Spells|TanisKatsakisTanithou/Magic#Spells]]

----
[[!template id=divbox content="""
<a id=Casting" />
### Casting Table
Technique|Form|T+F|CS|LT|FC|SC|SS|C(NS)|C(S)|R|P|MR
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Creo|Animal|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Aquam|14|15|27|15+die|3|7.5+(dStress/2)|4.2|10+(dStress/2)|20+dStress|5|6
 |Auram|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Corpus|14|15|27|15+die|3|7.5+(dStress/2)|4.2|10+(dStress/2)|20+dStress|5|6
 |Herbam|15|16|28|16+die|3|8+(dStress/2)|4.2|11.5+(dStress/2)|21+dStress|10|12
 |Ignem|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Imaginem|20|21|33|21+die|4|10.5+(dStress/2)|5.2|13+(dStress/2)|26+dStress|5|8
 |Mentem|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Terram|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Vim|20|21|33|21+die|4|10.5+(dStress/2)|5.2|13+(dStress/2)|26+dStress|5|8
&nbsp;||||||||||||

Intellego|Animal|16|17|29|17+die|3|8.5+(dStress/2)|4.2|11+(dStress/2)|22+dStress|5|7
 |Aquam|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|6
 |Auram|16|17|29|17+die|3|8.5+(dStress/2)|4.2|11+(dStress/2)|22+dStress|5|7
 |Corpus|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|6
 |Herbam|16|17|29|17+die|3|8.5+(dStress/2)|4.2|11+(dStress/2)|22+dStress|10|12
 |Ignem|16|17|29|17+die|3|8.5+(dStress/2)|4.2|11+(dStress/2)|22+dStress|5|7
 |Imaginem|21|22|34|22+die|4|11+(dStress/2)|5.2|13.5+(dStress/2)|27+dStress|5|8
 |Mentem|16|17|29|17+die|3|8.5+(dStress/2)|4.2|11+(dStress/2)|22+dStress|5|7
 |Terram|16|17|29|17+die|3|8.5+(dStress/2)|4.2|11+(dStress/2)|22+dStress|5|7
 |Vim|21|22|34|22+die|4|11+(dStress/2)|5.2|13.5+(dStress/2)|27+dStress|5|8
&nbsp;||||||||||||

Muto|Animal|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Aquam|14|15|27|15+die|3|7.5+(dStress/2)|4.2|10+(dStress/2)|20+dStress|5|6
 |Auram|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Corpus|14|15|27|15+die|3|7.5+(dStress/2)|4.2|10+(dStress/2)|20+dStress|5|6
 |Herbam|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|10|12
 |Ignem|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Imaginem|20|21|33|21+die|4|10.5+(dStress/2)|5.2|13+(dStress/2)|26+dStress|5|8
 |Mentem|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Terram|15|16|28|16+die|3|8+(dStress/2)|4.2|10.5+(dStress/2)|21+dStress|5|7
 |Vim|20|21|33|21+die|4|10.5+(dStress/2)|5.2|13+(dStress/2)|26+dStress|5|8
&nbsp;||||||||||||

Perdo|Animal|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|7
 |Aquam|11|12|24|12+die|2|6+(dStress/2)|3.2|8.5+(dStress/2)|17+dStress|5|6
 |Auram|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|7
 |Corpus|11|12|24|12+die|2|6+(dStress/2)|3.2|8.5+(dStress/2)|17+dStress|5|6
 |Herbam|12|13|25|13+die|3|6.5+(dStress/2)|4.2|;+(dStress/2)|18+dStress|10|12
 |Ignem|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|7
 |Imaginem|17|18|30|18+die|4|9+(dStress/2)|5.2|11.5+(dStress/2)|23+dStress|5|8
 |Mentem|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|7
 |Terram|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|7
 |Vim|17|18|30|18+die|4|9+(dStress/2)|5.2|11.5+(dStress/2)|23+dStress|5|8
&nbsp;||||||||||||

Rego|Animal|13|14|26|14+die|3|7+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|5|7
 |Aquam|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|6
 |Auram|13|14|26|14+die|3|7+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|5|7
 |Corpus|12|13|25|13+die|3|6.5+(dStress/2)|4.2|9+(dStress/2)|18+dStress|5|6
 |Herbam|13|14|26|14+die|3|7+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|10|12
 |Ignem|13|14|26|14+die|3|7+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|5|7
 |Imaginem|18|19|31|19+die|4|9.5+(dStress/2)|5.2|12+(dStress/2)|24+dStress|5|8
 |Mentem|13|14|26|14+die|3|7+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|5|7
 |Terram|13|14|26|14+die|3|7+(dStress/2)|4.2|9.5+(dStress/2)|19+dStress|5|7
 |Vim|18|19|31|19+die|4|9.5+(dStress/2)|5.2|12+(dStress/2)|24+dStress|5|8
"""]]
[[!template id=divbox content="""
<a id="CTNotes" />
### Casting Table Notes
Abbreviation | Full | Explanation
:--|:--|:--
T+F | Technique plus Form | 
CS | Casting Score | Base magical power without modifiers
FC | Formulaic Casting | Highest level of non-ritual spell castable (max level 45)
LT | Lab Total | Highest level of spell learnable
SC | Spontaneous Casting | Highest level of spell castable spontaneously without fatigue
SS | Stressed Spontaneous  | Highest level of spell castable spontaneously by taking fatigue
C (NS) | Ceremonial Casting | Take 15 minutes per magnitude to boost spontaneous casting
C (S) | Ceremonial Casting | Take 15 minutes per magnitude to boost stressed spontaneous casting
R | Ritual Casting | Ritual spells take longer but can be higher level than formulaic
P | Parma Magica | 
MR | Magic Resistance | 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Notes" />
### Notes
* If a form or technique is Deficient, halve all casting and lab totals that use that art after calculating as above. This includes halving the dice rolls.
* If you have Flawed Parma Magica, your Parma Magica is halved for the form in which it is flawed
* If you have incompatible arts, delete the relevant row.
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Techniques" />
### Techniques
:--|:--
Creo|9
Intellego|10
Muto|9
Perdo|6
Rego|7
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Forms" />
### Forms
:--|:--
Animal|6
Aquam|5
Auram|6
Corpus|5
Herbam|6
&nbsp;|
"""]]
[[!template id=divbox content="""
### Forms
:--|:--
Ignem|6
Imaginem|11
Mentem|6
Terram|6
Vim|11
&nbsp;|
"""]]
<br style="clear:left;" />
<a id="Spells" />
### Spells
Spell Name|Art 1|Art 2|Requisites|Level|Type|Range|Duration|Target|Description|MXP|M|Mastered Abilities|Spell Use|Sigil Manifestation
:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--:|:--:|:--|:--|:--
[[Prismatic Henna|/Meta/Spells/PrismaticHenna]]|Muto|Imaginem| |5|Formulaic|Touch|Not Sleeping|Individual|Changes Hair Colour|0|0| |Appearance|Plants near the target bloom slightly 
[[Piercing the Faerie Veil|/Meta/Spells/PiercingtheFaerieVeil]]|Intellego|Vim| |20|Formulaic|Personal|While One Eye Closed|Vision|Allows you to see into Faerie Regiones|0|0| |Investigation|Plants near the caster bloom/grow
[[Transformation of the Thorny Staff|/Meta/Spells/TransformationoftheThornyStaff]]|Muto|Herbam| |10|Formulaic|Touch|Sun|Individual|Causes a length of wood to grow thorns|0|0| |Combat|Causes leaves to bud and slowly grow on the staff during the duration of the spell.
[[Piercing Staff of Wood|/Meta/Spells/PiercingStaffofWood]]|Muto|Herbam|Rego|10|Formulaic|Voice|Momentary|Individual|Turns a piece of wood into a shaft that attacks a target|0|0| |Offensive|The projectile is covered in a helix of vines
[[Rope of Bronze|/Meta/Spells/RopeofBronze]]|Muto|Herbam|Terram|15|Formulaic|Touch|Sun|Individual|Turns a piece of rope into bronze|0|0| |Utility|The bronze rope is inscribed with leaf patterns
[[Intuition of the Forest|/Meta/Spells/IntuitionoftheForest]]|Intellego|Herbam| |10|Formulaic|Touch|Sun|Group|+3 to nature-oriented rolls in a forest|0|0| |Utility|Parts of the forest near the group gain vitality
[[Ward against Faeries of the Wood|/Meta/Spells/WardagainstFaeriesoftheWood]]|Rego|Herbam| |20|Formulaic|Touch|Ring|Circle|Ward against wood faeries|0|0| |Ward|Plants grow around the boundary of the ward
[[Wizard's Sidestep|/Meta/Spells/WizardsSidestep]]|Rego|Imaginem| |10|Formulaic|Personal|Sun|Individual|Your image appears up to 1 pace from where you actually are|0|0| |Defensive|Plants in the area are slightly invigorated
[[Aura of Ennobled Presence|/Meta/Spells/AuraofEnnobledPresence]]|Muto|Imaginem| |10|Formulaic|Touch|Sun|Individual|+3 on rolls to influence, lead, or convince others|0|0| |Utility|Plants near the target bloom slightly
[[Slap of Awakening|/Meta/Spells/SlapofAwakening]]|Rego|Mentem| |5|Formulaic|Touch|Momentary|Individual|Wakes a sleeping target|0|0| |Utility|Plants near the target bloom slightly
[[The Smokeless Campfire|/Meta/Spells/TheSmokelessCampfire]]|Muto|Ignem| |5|Formulaic|Voice|Sun|Individual|This spell changes the nature of a fire so it does not produce smoke|0|0| |Utility|The flames of the fire become suggestive of leaves
[[Aegis of the Hearth|/Meta/Spells/AegisoftheHearth]]|Rego|Vim| |20|Ritual|Touch|Year|Bound|The Aegis|0|0| |Defensive|Plants around the boundary of the Aegis are invigorated
[[Speak with Plant and Tree|/Meta/Spells/SpeakwithPlantandTree]]|Intellego|Herbam| |25|Formulaic|Touch|Concentration|Individual|Speak with a Plant for one Conversation|0|0| |Utility|The plant is slightly invigorated
[[Image of the Distant Traveller|/Meta/Spells/ImageoftheDistantTraveller]]|Intellego|Imaginem| |15|Formulaic|Road|While Eyes are Closed|Group|While Tanis keeps her eyes closed, she can see and hear what is happening in the place designated.  The place must be on the same road as her and be either a location or a person.|0|0| |Investigation|Weeds sprout up in the road around Tanis; potentially in a significant pattern.
[[Distant Perception of the Maga's Soliloquy|/Meta/Spells/DistantPerceptionoftheMagasSoliloquy]]|Intellego|Imaginem| |15|Formulaic|Road|Concentration|Hearing|The Target, who Tanis must know is on the same Road as her, can hear her for the duration of the spell|0|0| |Utility|The target has a perception that Tanis is using flowery language.
[[Silence of the Smothered Sound|/Meta/Spells/SilenceoftheSmotheredSound]]|Perdo|Imaginem| |20|Formulaic|Voice|Sun|Individual|Makes one being or object incapable of producing sounds.|0|0| |Combat|The target is literally gagged with illusiary flowers
[[Revealing the Ties to the Stars|/Meta/Spells/RevealingtheTiestotheStars]]|Intellego|Vim| |25|Formulaic|Personal|Concentration|Vision|Makes items of astrological significance visible to Tanis|0|0| |Investigation|The significances are visible as plants or flowers around the object
[[Unbinding the Ermine's Voice|/Meta/Spells/UnbindingtheErminesVoice]]|Muto|Animal| |20|Bond|Touch|Concentration, Maintained|Individual|Allows Bright to talk!|0|0| |Bond Empowerment|Bright will tend towards flowery speech
[[Eyes of the Hawkmoth|/Meta/Spells/EyesoftheHawkmoth]]|Intellego|Imaginem| |25|Formulaic|Personal|Not Sleeping|Vision|Allows Tanis to see in complete darknes|0|0| |Utility|Plants and trees are particularly visible
[[Phantasm of the Human Form|/Meta/Spells/PhantasmoftheHumanForm]]|Creo|Imaginem| |25|Formulaic|Voice|Sun|Individual|Makes an image of a clothed and equipped person that can make noise|0|0| |Utility|The image is holding or wearing at least one plant
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
||||||||||||||
&nbsp;||||||||||||||
