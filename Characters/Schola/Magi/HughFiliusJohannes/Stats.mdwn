[[!template id=character name="Hugh ex Hexham filius Johannes" house="Guernicus" job="Apprentice" rank="Apprentice" appage=36 born=1220/06/22 updated=1271/12/22 covenant="Schola Pythagoranis" parens="Johannes " origin="Northumbria" ethnicity="English" religion="Catholic" age=51 image=""" [[!img Hugh size="200x" alt="Hugh"]] """]]
## Hugh ex Hexham filius Johannes

----
[[Main|HughFiliusJohannes]] / [[Magic|HughFiliusJohannes/Magic]] / [[Skills|HughFiliusJohannes/Skills]] / [[Stats|HughFiliusJohannes/Stats]] / [[Additional Detail|HughFiliusJohannes/AdditionalDetail]]

----
[[Miscellaneous|HughFiliusJohannes/Stats#Miscellaneous]] | [[Characteristics|HughFiliusJohannes/Stats#Characteristics]] | [[Weapons|HughFiliusJohannes/Stats#Weapons]] | [[Wounds|HughFiliusJohannes/Stats#Wounds]] | [[Fatigue|HughFiliusJohannes/Stats#Fatigue]] | [[Armour|HughFiliusJohannes/Stats#Armour]] | [[Other Combat|HughFiliusJohannes/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type|Mage| 
Gift|Gifted| 
Size|0| 
Decrepitude|0| 
Warping Score|2| 
Confidence Score|1| 
Confidence Points|3| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|4
Perception|1
Presence|1
Communication|4
Strength|1
Stamina|0
Dexterity|0
Quickness|0
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|No| |0|-2+dStress|No|No|0|0+dStress|No|No|Any
Fist|No|Brawl|No| |0|-2+dStress|0|0+dStress|0|0+dStress|0|1+dStress|Melee
Kick|No|Brawl|No| |-1|-3+dStress|0|0+dStress|-1|-1+dStress|3|4+dStress|Melee
Knife|No|Brawl|No| |0|-2+dStress|1|1+dStress|0|0+dStress|2|3+dStress|Melee
Longsword|No|Single Weapon|Yes|Longsword|2|0+dStress|4|11+dStress|1|8+dStress|6|7+dStress|Melee
Longsword and Enchanted Heater Shield (Aegis of Unbreakable Wood)|No|Single Weapon|Yes|Longsword|2|0+dStress|4|11+dStress|4|11+dStress|6|7+dStress|Melee
Enchanted longsword (Hardness of Adamantine, Edge of the Razor) and enchanted heater shield (Aegis of Unbreakable Wood)|No|Single Weapon|Yes|Longsword|2|0+dStress|4|11+dStress|4|11+dStress|9|10+dStress|Melee
|||||||||||||
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
Full Chain|6|9+dStress
Spells only (Veteran Soldier's Body, Doublet of Impenetrable Silk)|6|9+dStress
Chain and Spells (Veteran Soldier's Body, Doublet of Impenetrable Silk, Aegis of Unbreakable Wood, Hardness of Adamantine)|15|18+dStress
Partial leather scale|3|6+dStress
Leather and Spells (Veteran Soldier's Body, Doublet of Impenetrable Silk, Aegis of Unbreakable Wood, Hardness of Adamantine)|12|15+dStress
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|7
Burden|3
Encumbrance|2
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
