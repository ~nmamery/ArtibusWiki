[[!template id=character name="Godiuia Silver Tongue" house="Folk Witch" job="Signifer" rank="Milites" appage=26 born=1190/06/07 updated=1216/10/13 covenant="Schola Pythagoranis" parens="Ulveva of Fleamdyke" origin="East Anglia" ethnicity="Anglo-Saxon" religion="Catholic" age=26 ]]
## Godiuia Silver Tongue

----
[[Main|Godiuia]] / [[Skills|Godiuia/Skills]] / [[Stats|Godiuia/Stats]] / [[Additional Detail|Godiuia/AdditionalDetail]]

----
[[Main Skills Chart|Godiuia/Skills#Main]] | [[Other Skills Chart|Godiuia/Skills#Second]] | [[General Abilities|Godiuia/Skills#General]] | [[Academic Abilities|Godiuia/Skills#Academic]] | [[Lores|Godiuia/Skills#Lores]] | [[Crafts/Professions|Godiuia/Skills#Crafts]] | [[Arcane Abilities|Godiuia/Skills#Arcane]] | [[Martial Abilities|Godiuia/Skills#Martial]] | [[Other Abilities|Godiuia/Skills#Other]] | [[Languages|Godiuia/Skills#Languages]] | [[Supernatural Abilities|Godiuia/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No|Pigs|0|4|4|4|2|3|2|2
Athletics|No|Grace|2|6|6|6|4|5|4|4
Awareness|No| |-2|2|2|2|0|1|0|0
Bargain|No|Basic Foodstuffs|1|5|5|5|3|4|3|3
Brawl|No|Dodging|0|4|4|4|2|3|2|2
Carouse|No| |-2|2|2|2|0|1|0|0
Charm|Yes|First Impressions|5|9|9|9|7|8|7|7
Chirurgy|No| | | | | | | | | 
Concentration|No|Carefulness|0|4|4|4|2|3|2|2
Etiquette|No|Peasants|0|4|4|4|2|3|2|2
Folk Ken|No|Peasants|1|5|5|5|3|4|3|3
Guile |No|Elaborate Lies|0|4|4|4|2|3|2|2
Hunt|No| |-2|2|2|2|0|1|0|0
Intrigue|No|Gossip|0|4|4|4|2|3|2|2
Leadership|No|Synnove|-1|3|3|3|1|2|1|1
Legerdemain|No| | | | | | | | | 
Music|No| |-2|2|2|2|0|1|0|0
Ride|No|Staying On|-1|3|3|3|1|2|1|1
Stealth|No|Urban Areas|-1|3|3|3|1|2|1|1
Survival|No| |-2|2|2|2|0|1|0|0
Swim|No|Not Drowning|-1|3|3|3|1|2|1|1
Teaching|No| |-2|2|2|2|0|1|0|0
&nbsp;||||||||||

Artes Liberales|No|Rhetoric|-1|3|3|3|1|2|1|1
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No| | | | | | | | | 
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No| |-2|2|2|2|0|1|0|0
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |-2|2|2|2|0|1|0|0
Parma Magica|No| | | | | | | | | 
Penetration|No| |-2|2|2|2|0|1|0|0
&nbsp;||||||||||

Bows|No| |-2|2|2|2|0|1|0|0
Great Weapon|No| |-2|2|2|2|0|1|0|0
Single Weapon|No|Axe|3|7|7|7|5|6|5|5
Thrown Weapon|No| |-2|2|2|2|0|1|0|0
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Folk Witch Lore|No|Fleamdyke Coven|1|5|5|5|3|4|3|3
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

English|No|East Anglia|3|7|7|7|5|6|5|5
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Animal Ken|No|Pigs|1|5|5|5|3|4|3|3
Second Sight|No|Ghosts|1|5|5|5|3|4|3|3
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Folk Witch Magic Theory|No|Potions|1|5|5|5|3|4|3|3
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|2|Pigs
Athletics|4|Grace
Awareness|Yes| 
Bargain|3|Basic Foodstuffs
Brawl|2|Dodging
Carouse|Yes| 
Charm|5|First Impressions
Chirurgy|No| 
Concentration|2|Carefulness
Etiquette|2|Peasants
Folk Ken|3|Peasants
Guile |2|Elaborate Lies
Hunt|Yes| 
Intrigue|2|Gossip
Leadership|1|Synnove
Legerdemain|No| 
Music|Yes| 
Ride|1|Staying On
Stealth|1|Urban Areas
Survival|Yes| 
Swim|1|Not Drowning
Teaching|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|1|Rhetoric
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|No| 
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|5|Axe
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
:--|:--|:--
Folk Witch Lore|3|Fleamdyke Coven
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
English|5|East Anglia
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Animal Ken|3|Pigs
Second Sight|3|Ghosts
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
Folk Witch Magic Theory|3|Potions
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
