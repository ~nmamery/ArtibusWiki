[[!template id=character name="Henry Smallwood" house="NA" job="Optio" rank="Grog" appage=32 born=1187/08/29 updated=1219/09/01 covenant="Schola Pythagoranis" parens="NA" origin="Cambridge" ethnicity="English" religion="Christian" age=32 image=""" [[!img Henry size="200x" alt="Henry"]] """]]
## Henry Smallwood

----
[[Main|HenrySmallwood]] / [[Skills|HenrySmallwood/Skills]] / [[Stats|HenrySmallwood/Stats]] / [[Additional Detail|HenrySmallwood/AdditionalDetail]]

----
[[Main Skills Chart|HenrySmallwood/Skills#Main]] | [[Other Skills Chart|HenrySmallwood/Skills#Second]] | [[General Abilities|HenrySmallwood/Skills#General]] | [[Academic Abilities|HenrySmallwood/Skills#Academic]] | [[Lores|HenrySmallwood/Skills#Lores]] | [[Crafts/Professions|HenrySmallwood/Skills#Crafts]] | [[Arcane Abilities|HenrySmallwood/Skills#Arcane]] | [[Martial Abilities|HenrySmallwood/Skills#Martial]] | [[Other Abilities|HenrySmallwood/Skills#Other]] | [[Languages|HenrySmallwood/Skills#Languages]] | [[Supernatural Abilities|HenrySmallwood/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No| |0|1|0|-1|2|0|2|1
Athletics|No|Acrobatics|3|4|3|2|5|3|5|4
Awareness|No|Archery|2|3|2|1|4|2|4|3
Bargain|No| |0|1|0|-1|2|0|2|1
Brawl|No|Dodge|3|4|3|2|5|3|5|4
Carouse|No| |0|1|0|-1|2|0|2|1
Charm|No|Trustworthy|2|3|2|1|4|2|4|3
Chirurgy|No|Field Medicine|3|4|3|2|5|3|5|4
Concentration|No| |1|2|1|0|3|1|3|2
Etiquette|No| |0|1|0|-1|2|0|2|1
Folk Ken|No| |0|1|0|-1|2|0|2|1
Guile |No|Convincing|3|4|3|2|5|3|5|4
Hunt|No| |0|1|0|-1|2|0|2|1
Intrigue|No| |0|1|0|-1|2|0|2|1
Leadership|No| |0|1|0|-1|2|0|2|1
Legerdemain|No| | | | | | | | | 
Music|No|Song|1|2|1|0|3|1|3|2
Ride|No|Travelling|2|3|2|1|4|2|4|3
Stealth|No| |0|1|0|-1|2|0|2|1
Survival|No|Camping|1|2|1|0|3|1|3|2
Swim|No|Floating|1|2|1|0|3|1|3|2
Teaching|No| |0|1|0|-1|2|0|2|1
&nbsp;||||||||||

Artes Liberales|No|Grammar|3|4|3|2|5|3|5|4
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No| | | | | | | | | 
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No| |0|1|0|-1|2|0|2|1
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |0|1|0|-1|2|0|2|1
Parma Magica|No| | | | | | | | | 
Penetration|No| |0|1|0|-1|2|0|2|1
&nbsp;||||||||||

Bows|No|Longbow|4|5|4|3|6|4|6|5
Great Weapon|No|Staff|4|5|4|3|6|4|6|5
Single Weapon|No| |0|1|0|-1|2|0|2|1
Thrown Weapon|No| |0|1|0|-1|2|0|2|1
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
East Anglia||People|2|3|2|1|4|2|4|3
England||Road to Esk|1|2|1|0|3|1|3|2
University of Cambridge||Non-academics|2|3|2|1|4|2|4|3
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Soldier||Discipline|3|4|3|2|5|3|5|4
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

English||East Anglia|5|6|5|4|7|5|7|6
Latin||Academic|4|5|4|3|6|4|6|5
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Premonitions||Threats to the Schola|5|6|5|4|7|5|7|6
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|Yes| 
Athletics|3|Acrobatics
Awareness|2|Archery
Bargain|Yes| 
Brawl|3|Dodge
Carouse|Yes| 
Charm|2|Trustworthy
Chirurgy|3|Field Medicine
Concentration|1| 
Etiquette|Yes| 
Folk Ken|Yes| 
Guile |3|Convincing
Hunt|Yes| 
Intrigue|Yes| 
Leadership|Yes| 
Legerdemain|No| 
Music|1|Song
Ride|2|Travelling
Stealth|Yes| 
Survival|1|Camping
Swim|1|Floating
Teaching|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|3|Grammar
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|No| 
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|4|Longbow
Great Weapon|4|Staff
Single Weapon|Yes| 
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
:--|:--|:--
East Anglia|2|People
England|1|Road to Esk
University of Cambridge|2|Non-academics
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
English|5|East Anglia
Latin|4|Academic
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
:--|:--|:--
Soldier|3|Discipline
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Premonitions|5|Threats to the Schola
||
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
||
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
