[[!template id=character name="Oriel the Hound" house="NA" job="Turb" rank="Milites" appage=29 born=1208/03/08 updated=1237/03/01 covenant="Schola Pythagoranis" parens="NA" origin="Cambridge" ethnicity="Norman" religion="Catholic" age=29 image=""" [[!img Oriel size="200x" alt="Oriel"]] """]]
## Oriel the Hound

----
[[Main|OrielTheHound]] / [[Skills|OrielTheHound/Skills]] / [[Stats|OrielTheHound/Stats]] / [[Additional Detail|OrielTheHound/AdditionalDetail]]

----
[[Main Skills Chart|OrielTheHound/Skills#Main]] | [[Other Skills Chart|OrielTheHound/Skills#Second]] | [[General Abilities|OrielTheHound/Skills#General]] | [[Academic Abilities|OrielTheHound/Skills#Academic]] | [[Lores|OrielTheHound/Skills#Lores]] | [[Crafts/Professions|OrielTheHound/Skills#Crafts]] | [[Arcane Abilities|OrielTheHound/Skills#Arcane]] | [[Martial Abilities|OrielTheHound/Skills#Martial]] | [[Other Abilities|OrielTheHound/Skills#Other]] | [[Languages|OrielTheHound/Skills#Languages]] | [[Supernatural Abilities|OrielTheHound/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No|Hounds|2|3|4|2|4|3|3|3
Athletics|No|Endurance|3|4|5|3|5|4|4|4
Awareness|No|Countryside|2|3|4|2|4|3|3|3
Bargain|No|Haggling|1|2|3|1|3|2|2|2
Brawl|No|Dodge|1|2|3|1|3|2|2|2
Carouse|No|Flirtations|1|2|3|1|3|2|2|2
Charm|No|Defusing tension|1|2|3|1|3|2|2|2
Chirurgy|No|Household remedies|1|2|3|1|3|2|2|2
Concentration|No| |0|1|2|0|2|1|1|1
Etiquette|No|Courtly Love|1|2|3|1|3|2|2|2
Folk Ken|No|Trustworthy|1|2|3|1|3|2|2|2
Guile |No| |0|1|2|0|2|1|1|1
Hunt|No|Small Game|2|3|4|2|4|3|3|3
Intrigue|No| |0|1|2|0|2|1|1|1
Leadership|No|Commanding|4|5|6|4|6|5|5|5
Legerdemain|No| | | | | | | | | 
Music|No| |0|1|2|0|2|1|1|1
Ride|No| |0|1|2|0|2|1|1|1
Stealth|No|Woodlands|3|4|5|3|5|4|4|4
Survival|No|Foraging|2|3|4|2|4|3|3|3
Swim|No|Freshwater|2|3|4|2|4|3|3|3
Teaching|No| |0|1|2|0|2|1|1|1
&nbsp;||||||||||

Artes Liberales|No|Reading|2|3|4|2|4|3|3|3
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No| | | | | | | | | 
Theology|No|Works of the Devil|2|3|4|2|4|3|3|3
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No|Urban|0|1|2|0|2|1|1|1
Finesse|No| |0|1|2|0|2|1|1|1
Infernal Lore|No| | | | | | | | | 
Magic Lore|No|Regios|0|1|2|0|2|1|1|1
Magic Theory|No| |0|1|2|0|2|1|1|1
Parma Magica|No| | | | | | | | | 
Penetration|No| |0|1|2|0|2|1|1|1
&nbsp;||||||||||

Bows|No|Longbow|4|5|6|4|6|5|5|5
Great Weapon|No| |0|1|2|0|2|1|1|1
Single Weapon|No|Short Sword|2|3|4|2|4|3|3|3
Thrown Weapon|No| |0|1|2|0|2|1|1|1
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
East Anglia||Banks of the Cam|1|2|3|1|3|2|2|2
England||Road to Esk|1|2|3|1|3|2|2|2
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Profession: Soldier||Sergeant|4|5|6|4|6|5|5|5
Profession: Housewife||Makeshift|2|3|4|2|4|3|3|3
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Norman French||Cambridge|5|6|7|5|7|6|6|6
English||Cambridge|3|4|5|3|5|4|4|4
Latin||Church|1|2|3|1|3|2|2|2
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Shapeshifter||Wolfhound|2|3|4|2|4|3|3|3
Wilderness Sense||Foraging|3|4|5|3|5|4|4|4
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|2|Hounds
Athletics|3|Endurance
Awareness|2|Countryside
Bargain|1|Haggling
Brawl|1|Dodge
Carouse|1|Flirtations
Charm|1|Defusing tension
Chirurgy|1|Household remedies
Concentration|Yes| 
Etiquette|1|Courtly Love
Folk Ken|1|Trustworthy
Guile |Yes| 
Hunt|2|Small Game
Intrigue|Yes| 
Leadership|4|Commanding
Legerdemain|No| 
Music|Yes| 
Ride|Yes| 
Stealth|3|Woodlands
Survival|2|Foraging
Swim|2|Freshwater
Teaching|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|0|Urban
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|0|Regios
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|2|Reading
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|No| 
Theology|2|Works of the Devil
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|4|Longbow
Great Weapon|Yes| 
Single Weapon|2|Short Sword
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
:--|:--|:--
East Anglia|1|Banks of the Cam
England|1|Road to Esk
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
Norman French|5|Cambridge
English|3|Cambridge
Latin|1|Church
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
:--|:--|:--
Profession: Soldier|4|Sergeant
Profession: Housewife|2|Makeshift
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Shapeshifter|2|Wolfhound
Wilderness Sense|3|Foraging
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
||
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
