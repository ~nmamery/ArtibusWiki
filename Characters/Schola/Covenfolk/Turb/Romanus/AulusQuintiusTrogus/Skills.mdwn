[[!template id=character name="Aulus Quintius Trogus" house="NA" job="Decurion Romanus" rank="Grog" appage=25 born=1185/04/05 updated=1228/09/01 covenant="Schola Pyhtagoranis" parens="NA" origin="Roman Citizen" ethnicity="Roman" religion="Cult of Mithras" age=43 image=""" [[!img Aulus size="200x" alt="Aulus"]] """]]
## Aulus Quintius Trogus

----
[[Main|AulusQuintiusTrogus]] / [[Skills|AulusQuintiusTrogus/Skills]] / [[Stats|AulusQuintiusTrogus/Stats]] / [[Additional Detail|AulusQuintiusTrogus/AdditionalDetail]]

----
[[Main Skills Chart|AulusQuintiusTrogus/Skills#Main]] | [[Other Skills Chart|AulusQuintiusTrogus/Skills#Second]] | [[General Abilities|AulusQuintiusTrogus/Skills#General]] | [[Academic Abilities|AulusQuintiusTrogus/Skills#Academic]] | [[Lores|AulusQuintiusTrogus/Skills#Lores]] | [[Crafts/Professions|AulusQuintiusTrogus/Skills#Crafts]] | [[Arcane Abilities|AulusQuintiusTrogus/Skills#Arcane]] | [[Martial Abilities|AulusQuintiusTrogus/Skills#Martial]] | [[Other Abilities|AulusQuintiusTrogus/Skills#Other]] | [[Languages|AulusQuintiusTrogus/Skills#Languages]] | [[Supernatural Abilities|AulusQuintiusTrogus/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No|Pack Animals|0|1|1|2|3|3|1|2
Athletics|No|Marching|2|3|3|4|5|5|3|4
Awareness|No|In battle|2|3|3|4|5|5|3|4
Bargain|No|Drink|0|1|1|2|3|3|1|2
Brawl|No|Grappling|2|3|3|4|5|5|3|4
Carouse|No|Looking sober|0|1|1|2|3|3|1|2
Charm|No| |-1|0|0|1|2|2|0|1
Chirurgy|No| | | | | | | | | 
Concentration|No| |-1|0|0|1|2|2|0|1
Etiquette|No|Military|0|1|1|2|3|3|1|2
Folk Ken|No| |-1|0|0|1|2|2|0|1
Guile |No| |-1|0|0|1|2|2|0|1
Hunt|No| |-1|0|0|1|2|2|0|1
Intrigue|No| |-1|0|0|1|2|2|0|1
Leadership|No|Soldiers|1|2|2|3|4|4|2|3
Legerdemain|No| | | | | | | | | 
Music|No| |-1|0|0|1|2|2|0|1
Ride|No| |-1|0|0|1|2|2|0|1
Stealth|No| |-1|0|0|1|2|2|0|1
Survival|No|In a group|2|3|3|4|5|5|3|4
Swim|No|In armour|0|1|1|2|3|3|1|2
Teaching|No|Profession Legionary|4|5|5|6|7|7|5|6
&nbsp;||||||||||

Artes Liberales|No| | | | | | | | | 
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No| | | | | | | | | 
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No| |-1|0|0|1|2|2|0|1
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |-1|0|0|1|2|2|0|1
Parma Magica|No| | | | | | | | | 
Penetration|No| |-1|0|0|1|2|2|0|1
&nbsp;||||||||||

Bows|No| |-1|0|0|1|2|2|0|1
Great Weapon|No| |-1|0|0|1|2|2|0|1
Single Weapon|No|Gladius|4|5|5|6|7|7|5|6
Thrown Weapon|No|Pilum|3|4|4|5|6|6|4|5
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Roman Army||9th legion|2|3|3|4|5|5|3|4
Cambridge|| | | | | | | | | 
England||Roads|0|1|1|2|3|3|1|2
Syndexioi||Military|1|2|2|3|4|4|2|3
||||||||||
||||||||||
&nbsp;||||||||||

Legionary||Not getting caught|6|7|7|8|9|9|7|8
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Latin||Silver Age|4|5|5|6|7|7|5|6
English||East Anglia|3|4|4|5|6|6|4|5
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|1|Pack Animals
Athletics|3|Marching
Awareness|3|In battle
Bargain|1|Drink
Brawl|3|Grappling
Carouse|1|Looking sober
Charm|Yes| 
Chirurgy|No| 
Concentration|Yes| 
Etiquette|1|Military
Folk Ken|Yes| 
Guile |Yes| 
Hunt|Yes| 
Intrigue|Yes| 
Leadership|2|Soldiers
Legerdemain|No| 
Music|Yes| 
Ride|Yes| 
Stealth|Yes| 
Survival|3|In a group
Swim|1|In armour
Teaching|5|Profession Legionary
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|No| 
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|No| 
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|5|Gladius
Thrown Weapon|4|Pilum
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
:--|:--|:--
Roman Army|3|9th legion
Cambridge|No| 
England|1|Roads
Syndexioi|2|Military
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
Latin|5|Silver Age
English|4|East Anglia
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
:--|:--|:--
Legionary|7|Not getting caught
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
 
||
||
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
||
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
