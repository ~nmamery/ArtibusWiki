[[!template id=character name="Maui" house=" " job="Covenant Cat" rank=" " appage=2 born=1214/02/14 updated=1226/06/01 covenant="Schola Pythagoranis" parens=" " origin="Barcelona" ethnicity="Black Cat Lineage" religion="Feline" age=12 image=""" [[!img Maui.png size="200x" alt="Maui.png"]] """]]
## Maui

----
[[Main|TomBlack]] / [[Powers|TomBlack/Powers]] / [[Skills|TomBlack/Skills]] / [[Stats|TomBlack/Stats]] / [[Additional Detail|TomBlack/AdditionalDetail]]

----
[[Main Skills Chart|TomBlack/Skills#Main]] | [[Other Skills Chart|TomBlack/Skills#Second]] | [[General Abilities|TomBlack/Skills#General]] | [[Academic Abilities|TomBlack/Skills#Academic]] | [[Lores|TomBlack/Skills#Lores]] | [[Crafts/Professions|TomBlack/Skills#Crafts]] | [[Arcane Abilities|TomBlack/Skills#Arcane]] | [[Martial Abilities|TomBlack/Skills#Martial]] | [[Other Abilities|TomBlack/Skills#Other]] | [[Languages|TomBlack/Skills#Languages]] | [[Supernatural Abilities|TomBlack/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No| |0|2|1|1|-8|-1|2|6
Athletics|No|Contortions|3|5|4|4|-5|2|5|9
Awareness|Yes|At night (+2)|6|8|7|7|-2|5|8|12
Bargain|No| |0|2|1|1|-8|-1|2|6
Brawl|No|Teeth|2|4|3|3|-6|1|4|8
Carouse|No| |0|2|1|1|-8|-1|2|6
Charm|No|Cats|1|3|2|2|-7|0|3|7
Chirurgy|No| | | | | | | | | 
Concentration|No| |0|2|1|1|-8|-1|2|6
Etiquette|No|Hermetic|1|3|2|2|-7|0|3|7
Folk Ken|No|Queens|1|3|2|2|-7|0|3|7
Guile |No|Fast Talk|1|3|2|2|-7|0|3|7
Hunt|No|Mice|4|6|5|5|-4|3|6|10
Intrigue|No|Making suggestions|1|3|2|2|-7|0|3|7
Leadership|No|Cats|2|4|3|3|-6|1|4|8
Legerdemain|No| | | | | | | | | 
Music|No| |0|2|1|1|-8|-1|2|6
Ride|No|Speed|1|3|2|2|-7|0|3|7
Stealth|No|Shadowing|4|6|5|5|-4|3|6|10
Survival|No|Urban|1|3|2|2|-7|0|3|7
Swim|No| |0|2|1|1|-8|-1|2|6
Teaching|No|Philosophiae|2|4|3|3|-6|1|4|8
&nbsp;||||||||||

Artes Liberales|No|Rhetoric|3|5|4|4|-5|2|5|9
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No|Ethics|4|6|5|5|-4|3|6|10
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No|Telekinesis|0|2|1|1|-8|-1|2|6
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |0|2|1|1|-8|-1|2|6
Parma Magica|No| | | | | | | | | 
Penetration|No| |0|2|1|1|-8|-1|2|6
&nbsp;||||||||||

Bows|No| |0|2|1|1|-8|-1|2|6
Great Weapon|No| |0|2|1|1|-8|-1|2|6
Single Weapon|No| |0|2|1|1|-8|-1|2|6
Thrown Weapon|No| |0|2|1|1|-8|-1|2|6
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Latin|No|Academic Usage|4|6|5|5|-4|3|6|10
Cat|No|Poetry|5|7|6|6|-3|4|7|11
English|No|Slang|2|4|3|3|-6|1|4|8
Hebrew|| |2|4|3|3|-6|1|4|8
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Second Sight|No|Invisible Things|1|3|2|2|-7|0|3|7
Shapeshifting|No|Human, Crow|2|4|3|3|-6|1|4|8
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|Yes| 
Athletics|3|Contortions
Awareness|4|At night (+2)
Bargain|Yes| 
Brawl|2|Teeth
Carouse|Yes| 
Charm|1|Cats
Chirurgy|No| 
Concentration|Yes| 
Etiquette|1|Hermetic
Folk Ken|1|Queens
Guile |1|Fast Talk
Hunt|4|Mice
Intrigue|1|Making suggestions
Leadership|2|Cats
Legerdemain|No| 
Music|Yes| 
Ride|1|Speed
Stealth|4|Shadowing
Survival|1|Urban
Swim|Yes| 
Teaching|2|Philosophiae
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|0|Telekinesis
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|3|Rhetoric
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|4|Ethics
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|Yes| 
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
Latin|4|Academic Usage
Cat|5|Poetry
English|2|Slang
Hebrew|2| 
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Second Sight|1|Invisible Things
Shapeshifting|2|Human, Crow
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
||
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
