[[!template id=character name="Tristram ex Mercere" house="Mercere" job="Redcap" rank="Apprentice" appage=18 born=1256/10/14 updated=1275/06/13 covenant="Schola" parens="Jocelyn" origin=" " ethnicity="Celtic" religion=" " age=18 ]]
## Tristram ex Mercere

----
[[Main|TristramExMercere]] / [[Skills|TristramExMercere/Skills]] / [[Stats|TristramExMercere/Stats]] / [[Additional Detail|TristramExMercere/AdditionalDetail]]

----
[[Main Skills Chart|TristramExMercere/Skills#Main]] | [[Other Skills Chart|TristramExMercere/Skills#Second]] | [[General Abilities|TristramExMercere/Skills#General]] | [[Academic Abilities|TristramExMercere/Skills#Academic]] | [[Lores|TristramExMercere/Skills#Lores]] | [[Crafts/Professions|TristramExMercere/Skills#Crafts]] | [[Arcane Abilities|TristramExMercere/Skills#Arcane]] | [[Martial Abilities|TristramExMercere/Skills#Martial]] | [[Other Abilities|TristramExMercere/Skills#Other]] | [[Languages|TristramExMercere/Skills#Languages]] | [[Supernatural Abilities|TristramExMercere/Skills#Supernatural]]

----
[[!template id=divbox content="""
<a id=Main" />
### Main Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Animal Handling|No|Parrots|1|3|1|2|1|3|3|3
Athletics|No|Acrobatics|3|5|3|4|3|5|5|5
Awareness|No|Alertness|1|3|1|2|1|3|3|3
Bargain|No|Food|1|3|1|2|1|3|3|3
Brawl|No|Daggers|4|6|4|5|4|6|6|6
Carouse|No| |0|2|0|1|0|2|2|2
Charm|No|Guttersnipe|3|5|3|4|3|5|5|5
Chirurgy|No|Bandaging|1|3|1|2|1|3|3|3
Concentration|No| |0|2|0|1|0|2|2|2
Etiquette|No|Hermetic|3|5|3|4|3|5|5|5
Folk Ken|No|Magi|1|3|1|2|1|3|3|3
Guile |No|Lying to Authority|1|3|1|2|1|3|3|3
Hunt|No|Snares|1|3|1|2|1|3|3|3
Intrigue|No| |0|2|0|1|0|2|2|2
Leadership|No| |0|2|0|1|0|2|2|2
Legerdemain|No| | | | | | | | | 
Music|No| |0|2|0|1|0|2|2|2
Ride|No|Travelling|2|4|2|3|2|4|4|4
Stealth|No|Hide|1|3|1|2|1|3|3|3
Survival|No|Travelling|1|3|1|2|1|3|3|3
Swim|No|Fresh Water|1|3|1|2|1|3|3|3
Teaching|No| |0|2|0|1|0|2|2|2
&nbsp;||||||||||

Artes Liberales|No|Reading|4|6|4|5|4|6|6|6
Civil and Canon Law|No| | | | | | | | | 
Common Law|No| | | | | | | | | 
Medicine|No| | | | | | | | | 
Philosophiae|No| | | | | | | | | 
Theology|No| | | | | | | | | 
&nbsp;||||||||||

Code of Hermes|No| | | | | | | | | 
Dominion Lore|No| | | | | | | | | 
Faerie Lore|No| | | | | | | | | 
Finesse|No| |0|2|0|1|0|2|2|2
Infernal Lore|No| | | | | | | | | 
Magic Lore|No| | | | | | | | | 
Magic Theory|No| |0|2|0|1|0|2|2|2
Parma Magica|No| | | | | | | | | 
Penetration|No| |0|2|0|1|0|2|2|2
&nbsp;||||||||||

Bows|No| |0|2|0|1|0|2|2|2
Great Weapon|No| |0|2|0|1|0|2|2|2
Single Weapon|No| |0|2|0|1|0|2|2|2
Thrown Weapon|No| |0|2|0|1|0|2|2|2
&nbsp;||||||||||
"""]]
[[!template id=divbox content="""
<a id=Second" />
### Other Skills Chart
Skill|P?|Specialty|Int|Per|Pre|Com|Str|Sta|Dex|Qui
:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:
Area Lore: Stonehenge Tribunal||Cambridge|2|4|2|3|2|4|4|4
Organisation Lore: Order of Hermes||Stonehenge|1|3|1|2|1|3|3|3
Area Lore: Lost Lands||Geography|1|3|1|2|1|3|3|3
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Profession: Sailor||Ocean|2|4|2|3|2|4|4|4
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

English||Anglo-Norman|5|7|5|6|5|7|7|7
Latin||Hermetic|4|6|4|5|4|6|6|6
French|| | | | | | | | | 
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Second Sight||Regiones|3|5|3|4|3|5|5|5
Magic Sensitivity||Auras|1|3|1|2|1|3|3|3
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||

Hurled Weapon| |Knives|2|4|2|3|2|4|4|4
||||||||||
||||||||||
||||||||||
||||||||||
&nbsp;||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="General" />
### General Abilities
:--|:--|:--
Animal Handling|1|Parrots
Athletics|3|Acrobatics
Awareness|1|Alertness
Bargain|1|Food
Brawl|4|Daggers
Carouse|Yes| 
Charm|3|Guttersnipe
Chirurgy|1|Bandaging
Concentration|Yes| 
Etiquette|3|Hermetic
Folk Ken|1|Magi
Guile |1|Lying to Authority
Hunt|1|Snares
Intrigue|Yes| 
Leadership|Yes| 
Legerdemain|No| 
Music|Yes| 
Ride|2|Travelling
Stealth|1|Hide
Survival|1|Travelling
Swim|1|Fresh Water
Teaching|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Arcane" />
### Arcane Abilities
:--|:--|:--
Code of Hermes|No| 
Dominion Lore|No| 
Faerie Lore|No| 
Finesse|Yes| 
Infernal Lore|No| 
Magic Lore|No| 
Magic Theory|Yes| 
Parma Magica|No| 
Penetration|Yes| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Academic" />
### Academic Abilities
:--|:--|:--
Artes Liberales|4|Reading
Civil and Canon Law|No| 
Common Law|No| 
Medicine|No| 
Philosophiae|No| 
Theology|No| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Martial" />
### Martial Abilities
:--|:--|:--
Bows|Yes| 
Great Weapon|Yes| 
Single Weapon|Yes| 
Thrown Weapon|Yes| 
&nbsp;||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Lores" />
### Lores
:--|:--|:--
Area Lore: Stonehenge Tribunal|2|Cambridge
Organisation Lore: Order of Hermes|1|Stonehenge
Area Lore: Lost Lands|1|Geography
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Languages" />
### Languages
:--|:--|:--
English|5|Anglo-Norman
Latin|4|Hermetic
French|No| 
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Crafts" />
### Crafts/Professions
:--|:--|:--
Profession: Sailor|2|Ocean
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Supernatural" />
### Supernatural
:--|:--|:--
Second Sight|3|Regiones
Magic Sensitivity|1|Auras
||
||
||
||
||
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Abilities
:--|:--|:--
Hurled Weapon|2|Knives
||
||
||
||
&nbsp;||
"""]]
<br style="clear:left;" />
