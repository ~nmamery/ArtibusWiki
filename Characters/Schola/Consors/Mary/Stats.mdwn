[[!template id=character name="Mary" house="Folk Witch" job="Folk Witch" rank="Guest" appage=40 born=1158/05/17 updated=1216/10/13 covenant="Schola Pythagoranis" parens="Faith" origin="Faerie Regio" ethnicity="English" religion="Old Religion / Christian" age=58 image=""" [[!img Mary size="200x" alt="Mary"]] """]]
## Mary

----
[[Main|Mary]] / [[Magic|Mary/Magic]] / [[Skills|Mary/Skills]] / [[Stats|Mary/Stats]] / [[Additional Detail|Mary/AdditionalDetail]]

----
[[Miscellaneous|Mary/Stats#Miscellaneous]] | [[Characteristics|Mary/Stats#Characteristics]] | [[Weapons|Mary/Stats#Weapons]] | [[Wounds|Mary/Stats#Wounds]] | [[Fatigue|Mary/Stats#Fatigue]] | [[Armour|Mary/Stats#Armour]] | [[Other Combat|Mary/Stats#Other]]    

----
[[!template id=divbox content="""
<a id="Miscellaneous" />
### Miscellaneous
:--|:--|:--
Type| | 
Gift|Gifted| 
Size|0| 
Decrepitude|0| 
Warping Score|0| 
Confidence Score|1| 
Confidence Points|3| 
Faith Points|0| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Characteristics" />
### Characteristics
:--|:--
Intellect|1
Perception|0
Presence|-1
Communication|2
Strength|-2
Stamina|3
Dexterity|1
Quickness|0
&nbsp;|
"""]]
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Weapons" />
### Weapons
Weapons|P?|Ability|S?|Speciality| |Initiative+dStress| |Attack+dStress| |Defense+dStress| |Base Damage+dStress|Range
:--|:--|:--|:--|:--|:--:|:--|:--:|:--|:--:|:--|:--:|:--|:--
Dodge|No|Brawl|No| |0|0+dStress|No|No|0|0+dStress|No|No|Any
Fist|No|Brawl|No| |0|0+dStress|0|1+dStress|0|0+dStress|0|-2+dStress|Melee
Kick|No|Brawl|No| |-1|-1+dStress|0|1+dStress|-1|-1+dStress|3|1+dStress|Melee
Knife|No|Brawl|No| |0|0+dStress|1|2+dStress|0|0+dStress|2|0+dStress|Melee
|||||||||||||
|||||||||||||
|||||||||||||
|||||||||||||
&nbsp;|||||||||||||
"""]]
<br style="clear:left;" />
[[!template id=divbox content="""
<a id="Wounds" />
### Wounds
Wound Penalty| |0
:--|:--:|:--
Light|-1| 
Medium|-3| 
Heavy|-5| 
Incapacitated|-| 
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Fatigue" />
### Fatigue
Fatigue Penalty|0|Fresh
:--|:--:|:--
Short Term| | 
Long Term| | 
&nbsp;|
"""]]
[[!template id=divbox content="""
<a id="Armour" />
### Armour
Armour| |Soak
:--|:--:|:--
None|0|3+dStress
||
||
||
||
&nbsp;||
"""]]
[[!template id=divbox content="""
<a id="Other" />
### Other Combat
:--|:--
Load|0
Burden|0
Encumbrance|0
&nbsp;|
"""]]
<br style="clear:left;" />




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
<br style="clear:left;" />
