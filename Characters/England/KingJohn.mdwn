[[!template id=character name="John" house="Angevin" job="" rank="King of England" appage= born=1166/12/24 updated=1221/12/24 ethnicity="Anglo-Norman" religion="Catholic" age=55]]

John was healed on his deathbed by Angelo.

He has a wife, Isabella, and five children; Henry, Richard, Joan,
Isabella, and Eleanor.

As of the end of Spring 1217 he is preparing to depart Newark Castle;
with only minor side effects of his illness (he will be healed
entirely mid-Summer).

As of Winter 1223 he's still alive and ruling in England

He becomes very frail after an illness in Winter 1235/6 and dies in
Winter 1236/7.

He was succeded by his son Henry and survived by his other children
