Being an approach to Maximianus of Nigrasaxa about a book

From Alexios, Autumn 1216:
> To the esteemed Maximianus of Bonisagus, Senior Magus of the Covenant of Nigrasaxa, Doctor Alexios Constantine, Magus of the Schola Pythagoranis and Magus Ex Miscellanea, sends greetings on this, the fifth day of November in the twelve hundred and sixteenth year of our Lord. 
> 
> I am of the belief that you have written a most superior book on the workings of magic in relation to the body, Health and the True Function of the Human Form. 
> 
> I would be fascinated to learn what price you would set on either a copy of this book or access to it for season so that I, or one of my sodales, could copy it for our own library. 
>
> With best wishes for the continued health of both yourself and your covenant, I remain, sir, yours in friendship and amity; Alexios. > 

From Maximianus, Spring 1217:
> To Doctor Alexios Constantine, Magus of the Schola Pythagoranis and
  Magus Ex Miscellanea,
>
> Many thanks for the kind words of your earlier letter, and I must
  apologise for not responding sooner; but ...
>
> *[Three pages of excuses about his current lab work]*
>
> Needless to say I would of course be willing to let you have a copy
  of my book.  I would be remiss in my duties as a member of House
  Bonisagus were I to request payment for it, but I would greatly
  appreciate it were you to provide us with a volume of equivalent
  interest to enrich our library for the benefit of my younger
  sodales.
>
> Yours *[etc]* Maximianus of Nigrasaxa.

From Alexios, Summer 1217:
> To the esteemed Maximianus of Bonisagus, Senior Magus of the Covenant of Nigrasaxa, Doctor Alexios Constantine, Magus of the Schola Pythagoranis and Magus Ex Miscellanea, sends greetings on this, the fifth day of June in the twelve hundred and seventeenth year of our Lord. 
> 
> Thank you for your response to my inquiry. You stated that you would be willing to exchange a copy of your book for one of equal value. Following consultation with our librarian, the book we believe most likely to interest you and most equal in value to that which you have scribed is one written by Bonisagus himself, on the fundamentals of the Parma magica. If this would be of interest, then we would be delighted to exchange copies. 
> 
> Alternatively, we have copies of the roots of various arts and some truly wondrous copies of more mundane texts which if you would prefer, we would be happy to exchange. 
> 
> With best wishes for the continued health of both yourself and your covenant, I remain, sir, yours in friendship and amity; Alexios. 

From the Librarian of Nigrasaxa, Summer 1217:
> To Doctor Alexios Constantine, Magus of the Schola Pythagoranis and
  Magus Ex Miscellanea, 
> 
> Esteemed Sir,
>

> The Magus Maximianus of Bonisagus has passed on your communication
  to me and asks that I inform you that the work on the Parma Magica 
  that you suggest would be very appropriate.
>
> It will take us two seasons to produce an unbound copy of
  Maximianus' book so we will be able to entrust that to the Redcaps for
  delivery in Spring.
>
> Our volume will be covered by the Bonisagus varient of the Cow and
  Calf Oath; with Nigrasaxa as the source covenant.  I assume that
  your volume will likewise be covered; can you please include the
  particulars of that oath with the volume?
>
> Yours *[etc]*

From Alexios, Late Summer 1217 to the Librarian of Nigrasaxa
> Esteeemed Sir,
>
>Thank you for your letter which I was delighted to receive. I will make arrangements with our own library and the Mercere for an unbound copy of the work by Bonisagus to be delivered at an equivalent time.
>The work is indeed bound by the same variant Oath, with the source covenant being Durenmar. A copy will be provided with the book. 
> Yours in amity,
> Alexios

From Alexios to Maximianus; Autumn 1217 
> Sir,
> 
>I wished to write in person to thank you for agreeing to gift our covenant with a copy of your work on the magics of the human body. I am very much looking forward to perusing it. 
>
>*there follows a page of pleasantries they say much and mean little.*
>
>I wondered if I might presume on your patience further to ask if you would be interested in entering into a more protracted correspondence on the topic of the human body. I think that the focus of my own studies; which has involved largely the instantaneous transmission of the human body, may provide slightly different insights than your own. I would certainly appreciate your thoughts on my research and would be delighted to provide you with any thoughts of my own if that would be of interest to you. 
>
>If I were to give you the bare bones of my research to date with respect to this area; it would comprise the following matters. 
>
>*here follows several pages of notes on Alexios' understanding of corpus, largely focusing on how the body can be moved without damaging it.*
>
>I look forward to your response. 



*[GM Note: It will take one season to bind the incoming volume; so it will 
be available to study from starting in Summer 1218.  The Parma Magica book 
is indeed under said Oath, with the source covenant being the Library of Durenmar]*

> *[Question to GM: given the quality of this book, would it make sense to incorporate resonant materials in this book? It seems to be sufficiently high level to be worth it to me and corpus is a very useful art.]*

> *[second question to GM: I have no idea what the final quality will be. Is the quality you gave originally just for the text and anything from the binding is extra? Ie if we get it bound by skilled artisans and incorporate resonant materials is it 15 as he had it, 16 for the resonant materials or 19 for skilled artisans and resonant materials?]*

*[GM: The original number is including skilled artisans but no resonant effects.  If you add one level of resonant materials then that will make it 16.]*

*[GM: Maximillian is willing to engage in correspondance, however his primary focus is very much in healing and he's quite busy; so you'll probably only manage one letter a season each way]*
