From Tanis, Autumn 1218, to Sachiko of Burnham, Jonaquil of Libellus,
Morlen of Nigrasaxa, Espera of Ungulus & Phessallia of Voluntas (five
seperate letters, all identical)
> Blessings be upon you,
> 
> I'm sure you will have heard of my parens, Elly of Animus Silvae in
> Novgorod.  Sadly it appears that she is no-longer with us; and as
> such I find myself with limited guidance in the ways of our house.
>
> As such I am writing to determine if you would be interested in
> discussion of our house and of other matters magical; whether in
> person or by regular correspondence.
>
> Yours in friendship,
>
> Tanis filia Elly, of the Followers of Pendule.
