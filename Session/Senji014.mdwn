## Date

* 18th June 2016

## Stories

* [[Story/Story027]]
* [[Story/Story027/Chapter001]]

## Setup

*Description of the state of the story, or the prerequisites for the
 session go here*
 
## Players should ensure they have

* Magi characters readyish

## Attendance

* [[Users/Ilanin]]
* [[Users/Senji]] (Storyguide)
* [[Users/Shadow]]
* [[Users/Zavier]] (via Skype)
