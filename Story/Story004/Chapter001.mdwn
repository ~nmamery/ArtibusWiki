# *The kittens are missing!*

----

## Summary

* Kittens stolen
* Kittens rescued
* There is a Panther in Angelo's lab
* Raffaelo bargains with Panther and allows Panther to stay in Angelo's lab on condition that he guards the lab and serves Angelo for as long as Angelo lives. 
* A witch is talked with and offered a home
* A faerie earl is talked with and swaps names and titles
* A faerie hunter is found to be the source of the path that allowed the kittens to be stolen. 

## Timing

This story ran from [[26 May 1218|Date/1218/05/26]] until [[28 August 1218|Date/1218/08/28]]


The story was in sessions:

* [[Session/Zavier001]]
* [[Session/Zavier002]]


## Cast

* Storyguide
    * [[Users/Zavier]]
* Magi
    * [[Alexios|Characters/Schola/Magi/AlexiosKaloethes]]
    * [[Angelo|Characters/Schola/Magi/AngeloexMercere]]
    * [[Johannes|Characters/Schola/Magi/JohannesExGuernicus]]
    * [[Tanis|Characters/Schola/Magi/TanisKatsakisTanithou]]
* Consors
    * [[Maui|Characters/Schola/Consors/TomBlack]]
* Grogs
    * [[Pebbles|Characters/Schola/Covenfolk/Cats/Pebbles/]]
    * [[Little Wanderer|Characters/Schola/Covenfolk/Cats/LittleWanderer/]]
    
## Detail

>A dark figure chirrups at six kittens and shows then milk. She leaves leaving only a single solitary reed and a damp smell. Maui and his band of feline rescuers follow them into the lion's den and out of it into a faerie regio which is the home of a witch, vicious fenlings (tiny faeries with thorned bodies and a sadistic attitude) and clouds that contain a castle in the sky. Confronting a mortally wounded witch, the cats discover she stole the kittens because she was lonely but was attacked by TheBeastOfTheFens who nearly killed her and in the fight, the kittens scattered. Eventually, 5 of the 6 kittens are rescued, two were being taught to hunt by the Beast, two had become the favourite pets of the son of the Earl of Clouds and two had been captured by fenlings. Of the last, only one survived. Maui finished the story by handing back the dead kitten to the witch and confronting her with the results of her selfishness. She wept. 

>Some time later, the mages follow up on the concerning knowledge that a witch could use faerie power to enter their covenant. After binding TheBeastOfTheFens, whose lair is a regio within Angelo's lab, through the bargaining skills of Alexios' secretary, the mages' enter the faerie regio to find that the witch, who has recovered faster than she should normally, was given a boon by a faerie hunter who she met in the fens. The Earl of English Clouds is visited but although he trades the information that the Hunter is not of his court and is a visitor to the Regio of the Fens, he is unable to tell them who exactly he is. Leaving his court, the Wizards collect the witch and take her back to be part of the covenant, on the grounds that this makes her less likely to be tricked again. She is excited although afraid as she had thought she would be killed by any member of the houses of Hermes. As a thank you, she offers them her most prized possessions; two books. The Wizards recognise these as having links to the marched Diedne house. 

>And, in conclusion, Maui takes the Sidhe lordling back his kittens. The young faerie is delighted and his father promises Maui a boon.

## Loot

* Little Wanderer gained faerie warping as did all 5 kittens
* One kitten gained immunity to drowning (minor faerie virtue)
* Maui gained the promise of a Boon from the Earl of English Clouds. 
* The covenant gained a Folk Witch (Mary)
* Angelo gained a very large Panther as guardian to his lab and also a regio extension that opens into a Faerie Fen. 
* The covenant gained access to three books owned by Mary; a Witch's Cookbook (which she won't let them have), and two books by Edith ex Diedne; On Our House (with glosses on the Schism War) and On Our Magic. 
