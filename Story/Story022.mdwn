# *Story Title Goes Here*

----

## Summary

*A summary of the events of the story go here.*

## Timing

This story ran from [[26th September 1216|Date/1216/09/26]] until
[[30th September 1216|Date/1216/09/30]].

It was in sessions:

* [[Session/Senji001]]
* [[Session/Senji002]]

## Cast

* Storyguide
    * [[Users/Senji]]
* Magi
    * [[Alexios|Characters/Schola/Magi/AlexiosKaloethes]]
    * [[Angelo|Characters/Schola/Magi/AngeloExMercere]]
    * [[Johannes|Characters/Schola/Magi/JohannesExGuernicus]]
    * [[Tanis|Characters/Schola/Magi/TanisKatsakisTanithou]]
* Consors
    * [[Fergus|Characters/Schola/Consors/FergusmacÓengus]]
    * [[Gaius|Characters/Schola/Consors/GaiusValeriusCorvus]]
    * [[Maui|Characters/Schola/Consors/TomBlack]]
    * [[Robert|Characters/Schola/Consors/RobertdeDovre]]
* Grogs
    * [[Grex Celtae|Characters/Schola/Covenfolk/Turb/Celtae]]
    * [[Grex Graecus|Characters/Schola/Covenfolk/Turb/Graecus]]
    * [[Grex Iudaeus|Characters/Schola/Covenfolk/Turb/Iudaeus]]
    * [[Grex Romanus|Characters/Schola/Covenfolk/Turb/Romanus]]
    
## Detail

*Detailed description of events goes here.*
