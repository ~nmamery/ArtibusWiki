### [[Arts Tractatus|ArtsTractatus]]

Classmark            | Ability     | Quality | Language | Title                                                        | Author                       | Type         | Additional Details
---------------------|------------:|:-------:|:--------:|:-------------------------------------------------------------|:-----------------------------|:-------------|:-------------------
[[F.1025]]           | Animal      |    13   |  Latin   | ***Anatomy of a Dragon***                                          | Vahakn ex Bonisagus          | Normal       | 
[[F.1174]]           | Aquam       |    14   |  Latin   | ***A Little Lake Magic***                                          | Tempestas ex Bonisagus       | Normal       | 
[[F.1181]]           | Aquam       |    14   |  Latin   | ***A Little More Lake Magic***                                     | Tempestas ex Bonisagus       | Normal       | 
[[F.1200]]           | Aquam       |    15   |  Latin   | ***Practical Hydrodynamics***                                      | Pietro ex Bonisagus          | Normal       | 
[[F.0948]]           | Auram       |    7    |  Latin   | ***Notes on The Incantation of Lightning***                        | Arnold ex Bonisagus          | Normal       | 
[[F.0997]]           | Auram       |    9    |  Latin   | ***Notes on Fog of Confusion***                                    | Arnold ex Bonisagus          | Normal       | 
[[F.1060]]           | Auram       |    13   |  Latin   | ***Thunder and Lightning***                                        | Frederick ex Bonisagus       | Normal       | 
[[F.1151]]           | Corpus      |    5    |  Latin   | ***Notes on Bane of the Decrepit Body***                           | Anita ex Bonisagus           | Normal       | 
[[F.1207]]           | Corpus      |    14   |  Latin   | ***A Few Notes on Transformation Magic***                          | Iacula ex Bonisagus          | Normal       | 
[[T.FCo.003]]        | Corpus      |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.FCo.004]]        | Corpus      |    6    |  Latin   |                                                                    |                              | Normal       | 
[[T.FCo.005]]        | Corpus      |    8    |  Latin   |                                                                    |                              | Normal       | 
[[T.FCo.K01]]        | Corpus      |    9    |  Latin   | ***Motion of the Body Vol. 1 - Muscles and Motion*** | Alexios Kaloethes & Maximianus ex Bonisagus | Correspondence | 
[[T.FCo.K02]]        | Corpus      |    9    |  Latin   | ***Motion of the Body Vol. 2 - Blood and Behaviour***| Alexios Kaloethes & Maximianus ex Bonisagus | Correspondence | 
[[T.FCo.006]]        | Corpus      |   11    |  Latin   | ***Humores***                                                      | Galen ex Jerbiton            | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Corpus      |   11    |  Latin   | ***Et Sanguis Bilis***                                             | Galen ex Jerbiton            | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Corpus      |   11    |  Latin   | ***Diagnosis Injuriam***                                           | Galen ex Jerbiton            | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Corpus      |   13    |  Latin   | ***Forma Hominis***                                                | Vladislav ex Tremere         | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Salutem     |    8    |  Latin   | ***Sicut Animalis Homo***                                          | Leonardo of Bologna          | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Salutem     |    8    |  Latin   | ***Processibus de Senescit***                                      | Leonardo of Bologna          | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Salutem     |    9    |  Latin   | ***Opus ut Membra***                                               | Pietro of Paris              | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.006]]        | Salutem     |   10    |  Latin   | ***Pulmones et Fimo Flammae***                                     | Lucio of Bologna             | Florilegium  | ***Omnia Corpori*** | The Council of Hippocrates |
[[T.FCo.007]]        | Corpus      |    10   |  Latin   | ***Why the Body is not like an Oak Tree***                         | Tanis ex Merinita            | Normal       | Accession: Su1262
[[T.FCo.008]]        | Corpus      |    10   |  Latin   | ***Why the Body is like an Oak Tree***                             | Tanis ex Merinita            | Normal       | Accession: Su1264
[[T.FCo.009]]        | Corpus      |    10   |  Latin   | ***The Attributes Without Mirror the Powers Within***              | Tanis ex Merinita            | Normal       | Accession: Su1267
[[F.1046]]           | Creo        |    5    |  Latin   | ***Notes of Wall of Protecting Stone***                            | Éua ex Bonisagus             | Normal       | 
[[T.TCr.002]]        | Creo        |    5    |  Latin   |                                                                    |                              | Normal       | 
[[T.TCr.003]]        | Creo        |    10   |  Latin   |                                                                    |                              | Normal       | 
[[T.TCr.004]]        | Creo        |    7    |  Latin   |                                                                    |                              | Normal       | 
[[T.TCr.005]]        | Creo        |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.TCr.006]]        | Creo        |    8    |  Latin   | ***Improvements***                                                 | Celestine filia Angelo & Hugh filius Johannes |Correspondance | 
[[T.TCr.007]]        | Creo        |    13   |  Latin   | ***Making Is What***                                               | Edward of Milton             | Normal       | 
[[T.TCr.008]]        | Creo        |    7    |  Latin   | ***Accelerating Nature*** ⁂ (Includes Breakthrough)                | Angelo ex Mercere            | Normal       | 
[[T.TCr.009]]        | Creo        |    10   |  Latin   | ***Projecting Shadows into the World***                            | Tanis ex Merinita            | Normal       | Accession: Wi1265
[[T.TCr.010]]        | Creo        |    12   |  Latin   | ***The Power of Vis***                                             | Alexios Kaloethes            | Normal       | Accession: Su1265
[[T.TCr.011]]        | Creo        |    10   |  Latin   | ***Perfection is Eternal***                                        | Tanis ex Merinita            | Normal       | Accession: Su1266
[[T.TCr.012]]        | Creo        |    12   |  Latin   | ***The Water of Life***                                            | Alexios Kaloethes            | Normal       | Accession: Su1267
[[F.1052]]           | Herbam      |    14   |  Latin   | ***Some Applications of Plant Magic to Self Defense***             | Anne Arbor ex Bonisagus      | Normal       | 
[[F.1186]]           | Herbam      |    15   |  Latin   | ***Root and Branch; A Summary of Gardening Magic***                | Pietro ex Bonisagus          | Normal       | 
[[T.FHe.002]]        | Herbam      |    8    |  Latin   |                                                                    |                              | Normal       | 
[[T.FHe.004]]        | Herbam      |    9    |  Latin   |                                                                    |                              | Normal       | 
[[F.1151]]           | Ignem       |    6    |  Latin   | ***Notes on Wizard's Icy Grip***                                   | Nuño ex Bonisagus            | Normal       | 
[[T.FIg.001]]        | Ignem       |    11   |  Latin   |                                                                    |                              | Normal       | 
[[T.FIg.003]]        | Ignem       |    8    |  Latin   |                                                                    |                              | Normal       | 
[[T.FIg.004]]        | Ignem       |    9    |  Latin   |                                                                    |                              | Normal       | 
[[F.0990]]           | Imaginem    |    11   |  Latin   | ***How You Can See Right Through Me***                             | Vestiarium ex Bonisagus      | Normal       | 
[[F.1116]]           | Imaginem    |    10   |  Latin   | ***Notes on Illusion of the Misplaced Castle***                    | Ximeno ex Bonisagus          | Normal       | 
[[F.1144]]           | Imaginem    |    15   |  Latin   | ***Culinary Magic III: Heightening the Senses***                   | Julius ex Bonisagus          | Normal       | 
[[T.FIm.003]]        | Imaginem    |    10   |  Latin   |                                                                    |                              | Normal       | 
[[T.FIm.005]]        | Imaginem    |    8    |  Latin   |                                                                    |                              | Normal       | 
[[T.FIm.006]]        | Imaginem    |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.FIm.007]]        | Imaginem    |    8    |  Latin   | ***Veiling the Sight; Correspondences with a Fox I***     | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.008]]        | Imaginem    |    8    |  Latin   | ***Veiling the Sound; Correspondences with a Fox II***    | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.009]]        | Imaginem    |    8    |  Latin   | ***Veiling the Smell; Correspondences with a Fox III***   | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.010]]        | Imaginem    |    8    |  Latin   | ***Veiling the Touch; Correspondences with a Fox IIII***  | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.011]]        | Imaginem    |    8    |  Latin   | ***Veiling the Taste; Correspondences with a Fox V***     | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.012]]        | Imaginem    |    8    |  Latin   | ***Creating the Image; Correspondences with a Fox VI***   | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.013]]        | Imaginem    |    8    |  Latin   | ***Knowing the Image; Correspondences with a Fox VII***   | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.014]]        | Imaginem    |    8    |  Latin   | ***Moving the Taste; Correspondences with a Fox VIII***   | Tanis ex Merinita & Vitenka the Fox | Correspondance | 
[[T.FIm.015]]        | Imaginem    |    4    |  Latin   | ***Disguising Bad Cooking***                                       | Johannes ex Guernicus & Findabair Ex Misc | Correspondance | 
[[F.1095]]           | Intellego   |    11   |  Latin   | ***Tips for the Curious Maga***                                    | Phyllis ex Bonisagus         | Normal       | 
[[T.TIn.001]]        | Intellego   |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.TIn.003]]        | Intellego   |    7    |  Latin   | ***On Using Magic to Understand Magic***                           | Johannes ex Guernicus & Verena Ex Criamon | Correspondance | 
[[T.TIn.004]]        | Intellego   |    7    |  Latin   | ***Intellego: The Foundation of Magic***                           | Johannes ex Guernicus & Verena Ex Criamon | Correspondance | 
[[T.TIn.005]]        | Intellego   |    7    |  Latin   | ***Intellego: The Foundation of Understanding***                   | Johannes ex Guernicus & Verena Ex Criamon | Correspondance | 
[[T.TIn.006]]        | Intellego   |    7    |  Latin   | ***Ranges and Targets in Intellego Magic***                        | Johannes ex Guernicus & Verena Ex Criamon | Correspondance | 
[[T.TIn.007]]        | Intellego   |    7    |  Latin   | ***Durations Discussed: Momentary versus Concentration in Intellego Magic*** | Johannes ex Guernicus & Verena Ex Criamon | Correspondance | 
[[T.TIn.KT1]]        | Intellego   |   10    |  Latin   | ***Reading the Leaves Vol. 1***                      | Alexios Kaloethes & Tanis ex Merenita     | Correspondence | 
[[T.TIn.KT2]]        | Intellego   |   10    |  Latin   | ***Reading the Leaves Vol. 2***                      | Alexios Kaloethes & Tanis ex Merenita     | Correspondence | 
[[T.TIn.008]]        | Intellego   |    13   |  Latin   | ***Finding What Is***                                              | Edward of Milton             | Normal       | 
[[F.0962]]           | Mentem      |    9    |  Latin   | ***The Principles of Mental Communication***                       | Lodewig ex Bonisagus         | Normal       | 
[[F.1004]]           | Mentem      |    10   |  Latin   | ***Ghosts and Spirits***                                           | Pietro ex Bonisagus          | Normal       | 
[[F.1123]]           | Mentem      |    15   |  Latin   | ***Minds Within Minds: An Examination of Unknown Influences***     | Melisende of Monteverte      | Normal       | 
[[T.FMe.001]]        | Mentem      |    7    |  Latin   | ***How Language Works***                                           | Viator ex Mercere            | Normal       | 
[[T.FMe.002]]        | Mentem      |    7    |  Latin   | ***Mentem Tactics for Certamen***                                  | Camelia ex Tremere           | Normal       | 
[[T.FMe.003]]        | Mentem      |    8    |  Latin   | ***Overwhelming the Mind***                                        | Bartoumiéu ex Tytalus        | Normal       | 
[[T.FMe.004]]        | Mentem      |    12   |  Latin   | ***Principles of Working on Memories***                            | Astrolabe ex Jerbiton        | Normal       | 
[[T.FMe.005]]        | Mentem      |    9    |  Latin   | ***Mental Magics Prior to Bonisagus***                             | Gharad Ex Misc               | Normal       | 
[[T.FMe.006]]        | Mentem      |    9    |  Latin   | ***Motivations Unknown***                                          | Julia ex Jerbiton            | Normal       | 
[[T.FMe.007]]        | Mentem      |    9    |  Latin   | ***Why Do People Work Against Their Better Interests***?           | Julia ex Jerbiton            | Normal       | 
[[T.FMe.008]]        | Mentem      |    13   |  Latin   | ***Ghosts: Summoning***                                            | Edward of Milton             | Normal       | 
[[T.FMe.009]]        | Mentem      |    13   |  Latin   | ***Ghosts: Coercing***                                             | Edward of Milton             | Normal       | 
[[T.FMe.010]]        | Mentem      |    13   |  Latin   | ***Ghosts: Questioning***                                          | Edward of Milton             | Normal       | 
[[T.FMe.011]]        | Mentem      |    13   |  Latin   | ***Ghosts: Destroying***                                           | Edward of Milton             | Normal       | 
[[T.FMe.012]]        | Mentem      |    13   |  Latin   | ***A Summary of the Field of Necromancy***                         | Edward of Milton             | Normal       | 
[[T.FMe.013]]        | Mentem      |    13   |  Latin   | ***How to Distract People***                                       | Edward of Milton             | Normal       | 
[[T.FMe.014]]        | Mentem      |    13   |  Latin   | ***The Workings of Language***                                     | Edward of Milton             | Normal       | 
[[F.1067]]           | Muto        |    16   |  Latin   | ***The Butterfly of Virtue: A Life History***                      | Papilio ex Bonisagus         | Normal       | 
[[F.1109]]           | Muto        |    14   |  Latin   | ***Practical Applications of Rain Magic***                         | Tempestas ex Bonisagus       | Normal       | 
[[F.1179]]           | Muto        |    11   |  Latin   | ***Correcting the Small Things***                                  | Milvia ex Bonisagus          | Normal       | 
[[T.TMu.003]]        | Muto        |    11   |  Latin   |                                                                    |                              | Normal       | 
[[T.TMu.004]]        | Muto        |    11   |  Latin   |                                                                    |                              | Normal       | 
[[T.TMu.006]]        | Muto        |    6    |  Latin   |                                                                    |                              | Normal       | 
[[T.TMu.007]]        | Muto        |    7    |  Latin   |                                                                    |                              | Normal       | 
[[T.TMu.008]]        | Muto        |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.TMu.009]]        | Muto        |    13   |  Latin   | ***A Few Notes on Transformation Magic***                          | Edward of Milton             | Normal       | 
[[T.TMu.010]]        | Muto        |    13   |  Latin   | ***Improving upon Nature***                                        | Edward of Milton             | Normal       | 
[[T.TMu.011]]        | Muto        |    13   |  Latin   | ***Having Permenent Effects with Temporary Magics***               | Edward of Milton             | Normal       | 
[[F.1011]]           | Perdo       |    11   |  Latin   | ***Revealing the Statue Within: Precision Perdo***                 | Antonio ex Bonisagus         | Normal       | 
[[F.1193]]           | Perdo       |    5    |  Latin   | ***The End of All Things***                                        | Ijon ex Bonisagus            | Normal       | 
[[T.TPe.002]]        | Perdo       |    10   |  Latin   |                                                                    |                              | Normal       | 
[[T.TPe.004]]        | Perdo       |    8    |  Latin   |                                                                    |                              | Normal       | 
[[F.1158]]           | Rego        |    7    |  Latin   | ***Progress Towards a More Effective Binding Spell***              | Grammaton ex Bonisagus       | Normal       | 
[[F.1172]]           | Rego        |    9    |  Latin   | ***Notes Towards a Method of Controlling Faeries***                | Palantus ex Bonisagus        | Normal       | 
[[F.1193]]           | Rego        |    7    |  Latin   | ***Manipulations of the Real***                                    | Demetrius ex Bonisagus       | Normal       | 
[[T.1088]]           | Rego        |    14   |  Latin   | ***There and Back Again***                                         | Talia ex Bonisagus           | Normal       | 
[[T.TRe.003]]        | Rego        |    6    |  Latin   |                                                                    |                              | Normal       | 
[[T.TRe.006]]        | Rego        |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.TRe.007]]        | Rego        |    9    |  Latin   |                                                                    |                              | Normal       | 
[[T.TRe.JK1]]        | Rego        |    8    |  Latin   | ***Magia Magicae Mutare Vol. 1***                    | Alexios Kaloethes & Johannes ex Guernicus | Correspondence | 
[[T.TRe.JK2]]        | Rego        |    8    |  Latin   | ***Magia Magicae Mutare Vol. 2***                    | Alexios Kaloethes & Johannes ex Guernicus | Correspondence | 
[[T.TRe.008]]        | Rego        |    13   |  Latin   | ***Getting Things Done (Part II)***                                | Edward of Milton             | Normal       | 
[[T.TRe.009]]        | Rego        |    13   |  Latin   | ***A Little Light Craftmagick***                                   | Edward of Milton             | Normal       | 
[[T.TRe.010]]        | Rego        |    13   |  Latin   | ***A Discussion on the Natural States of Being***                  | Edward of Milton             | Normal       | 
[[T.TRe.011]]        | Rego        |    13   |  Latin   | ***Doing Things with Accuracy (Part I)***                          | Edward of Milton             | Normal       | 
[[F.1193]]           | Terram      |    5    |  Latin   | ***Workings of the Flying Chariot of Alexandria***                 | Catella ex Bonisagus         | Normal       | 
[[T.FTe.002]]        | Terram      |    14   |  Latin   |                                                                    |                              | Normal       | 
[[T.FTe.003]]        | Terram      |    8    |  Latin   |                                                                    |                              | Normal       | 
[[T.FTe.004]]        | Terram      |    14   |  Latin   | ***What Lies Beneath***?                                           | Giovanni ex Bonisagus        | Normal       | 
[[T.FVi.001]]        | Vim         |    10   |  Latin   |                                                                    |                              | Normal       | 
&nbsp;|||||||||
