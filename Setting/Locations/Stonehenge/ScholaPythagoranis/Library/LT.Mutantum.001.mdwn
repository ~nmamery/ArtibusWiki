[[!template id=book name="Angelo's Collection of Mutantum Magic" author="Collated by Claudine" lang="Latin" subject="Lab Text" copyright="House Mercere" oath="Mutantes Only"]]

This book contains Mutantum spells copied from the library in Harco by
Claudine during Autumn 1221.

[[!inline pages="Meta/Spells/* and link(.)" show=0 raw=yes sort="title" ]]
