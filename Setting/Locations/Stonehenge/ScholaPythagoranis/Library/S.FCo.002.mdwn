[[!template id=book name="Health and True Function of the Human Form" author="Maximianus of Bonisagus" lang="Latin" subject="Corpus" level="15" quality="16" scribe="yes" binder="yes" illumination="yes" resonant="+1" copyright="Nigrasaxa" oath="Bonisagus and Calf"]]

This book was acquired from the covenant of Nigrasaxa in exchange for
[[The Parma Magica book|S.PaM.001]].

It is lovingly bound in Amber and Applewood.
