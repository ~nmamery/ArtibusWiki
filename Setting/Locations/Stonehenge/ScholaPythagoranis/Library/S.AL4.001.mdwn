[[!template id=book name="The Last Days of Lyonesse" author="" lang="Latin" subject="Area Lore: Lyonesse" level="3" quality="10" scribe="yes" binder="yes" illumination="yes"]]
[[!template id=book name="The Last Days of Lyonesse" author="" lang="Latin" subject="Magic Lore" quality="10" scribe="yes" binder="yes" illumination="yes"]]
[[!template id=book name="The Last Days of Lyonesse" author="" lang="Latin" subject="Dominion Lore" quality="10" scribe="yes" binder="yes" illumination="yes"]]

This book was found in a chest in the texts lab at the ex-covenant
at Stellasper.  

> ##### GM Note
> This book contains parts of the story of the *Prose Tristan* (excepting
> the Grail bits), the story of the sinking of Lyonesse, and various
> other tales set in Lyonesse.
