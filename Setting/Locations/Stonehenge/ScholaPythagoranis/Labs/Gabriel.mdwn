## Schoolhouse Laboratory

 

----

 

###Gabriel’s laboratory 

 

|Lab|Score|

|---:|:------------------------------------------------:|

|Size| +1 |

|Free Points|0|

|Refinement| 1|

|Occupied Size| +1 |

|Aura| 6 |

 

This lab is built in the tower at Enys Sampson and is a is a 680ft² circle with some cut outs where the stairs are; for a Size 1 base.

The lab itself is plainly decorated with bare wooden floors and walls. All areas have been carefully laid out in their own space which makes it easy to use and the equipment is of excellent quality. A large granite waterfall is carefully positioned on the west side of the laboratory, the water in this perpetually flows from the bottom to the top and is magically fresh and pure. 

 

|   Size |                                                              | GQ | Up | Safe | Warp | Heal | Æsth | Spec. 1.|Spec. 2 | Creo |  Rego  | Art 3 | Art 4  |

|---:|:-------------------------------------------------|:--:|:--:|:----:|:----:|:----:|:----:|:--------:|:----:|:---:|:-:|:----------:|:------:|:------:|:------:|

|  1 | Spacious         |       |         |   +2   |            |        |  +1    |        |        |       |           |          |                |

| 1 | Lesser Feature (Waterfall) |    |    |     |   |            |+1        |      |        |         |+1       |           |          |                |

|  0 | Superior Equipment                               | +1 | +2   | +1   |            |         |        |      |       |       |           |          |                |

|Highly Organised                                                    | +1 |      |      |            |         |        |      |        |       |           |          |                |

|  0 | Spotless                                                            |    |      |      |            |    +1   | +1     |   |        |       | +1           |          |                |

| -1 | Missing Equipment (Vis Extraction., Long. Rit.) |    | -1   |      |            |         |        |      |      |       |           |          |                |       

|  0 | Base Safety (Refinement – Occ. Size) |    |      | 0  |            |         |        |      |        |       |           |          |                |              

| &nbsp; ||||||||||||||

| +1 | **Totals**                                               |+2  |  +1  | +3   |    |  +1 |  +1 |    |      |  +1 |  +1       |        |           |   

| &nbsp; ||||||||||||||

|  0 | Familiar                                                     | +0|      | +0   |            |         |          |    |        |       |           |        |              |      

| &nbsp; ||||||||||||||

|   +1 | **Totals with familiar**                     | NA|    | NA   |            |    |     |       |      |     |         |      |        |     

| &nbsp; ||||||||||||||

[Lab Details]

 

 

 _Specialities not used:_

 

* NA
