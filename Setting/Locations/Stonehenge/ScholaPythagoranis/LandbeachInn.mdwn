The inn is situated on a parcel of land on both sides of the road:
including Landbeach lake.  
There is at least one kelpie in the lake.  Land on the lake side of
the road is lightly forested, and the other side is fenland.  The road
and inn itself are raised above the level of 10-year floods.

The inn is an old Roman Mansio.  The main building is in good repair,
but the hypocaust mechanism and bathhouse fell into disrepair
centuries ago.  The bathhouse has been converted into an ancillary inn
with a kosher kitchen for Jewish guests.  This version of the inn is
modelled as an Income source.  

The land is held in Copyhold from the Bishopric of Ely as "rent" the
inn is required to provide board and change of horses for agents of
the Bishopric.

There is also another version of the inn in the faerie regio on the
site (level 4).  This inn is mostly maintained by browies (think
enough milk to feed an entire army of hedgehogs, also some lares
rites) so the hypocaust (labs count as having Magical Heating) and
baths work.  The majority of the interior has been converted to labs;
and there is an outdoor lab.

There are two labs (33'x20',36'x22¼') converted out of storerooms at the front of the
building, behind the stables, and the other four (36'2"x17½') are in the sides.
The walkways in the courtyard on the sides have been filled in to make
auxilliary/bed rooms for those four labs, each of which has two 8½'x10'
rooms and three 8½'x5' rooms, one of which forms a small antichamber
for the lab itself.  The two front labs each have two associated adjacent
17½'x12' rooms (one above the other) off the courtyard, but there is no
direct access to them from the labs.  In general the walls of the
building are about 2' thick stone construction excepting those of the anciliary rooms
which have been lightly constructed out of brick.

On each side a stair between the front lab's rooms and the side labs
provides access to the upstairs walkway.  The upper floors of the side
labs have been removed to provide full height labs; but the doorways
are still there and could be reopened to provide access if desired.

The rear range includes kitchens and a dining room, and a number of
reception rooms.  The kitchens aren't generally used, food being
provided from the mundane inn, but are kept maintained in case of
emergencies.

The bath house has two complete sets of bathing rooms.  

The regio extends quite a distance; but has a hole where the Dominion
in Landbeach reaches level 4; you can walk out but not in at this
point -- probably into the church.

The Aegis in the regio is a long "sausage" shape in order to include
both the inn and the outdoor lab.

We could do with floorplans here.

The base auras at the inn and the road are:

* Faerie 1
* Divine 2

The divine falls off quickly to zero within yards of the road / inn
site.

The base aura in the regio is Faerie 4.

----

### Plans
* [[External view|inn-3d.pdf]]
* [Ground Floor](inn-ground.svg)
* [First Floor](inn-first-floor.svg)
* [Second Floor](inn-second-floor.svg)
* [Third Floor](inn-third-floor.svg)
