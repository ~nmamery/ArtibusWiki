Libellus is a Spring Covenant south of Bury St Edmunds.  It is part of
Voluntas' anti-Blackthorn alliance; but has done nothing else of note
yet.

* Stephen Eruditus of Jerbiton (leader)
* Meles of Bjornaer
* Gregorius of Bonisagus
* Lucidia of Flambeau
* Jonaquil of Merinita
* Justin of Tremere
* Fabricor of Verditius

Other than Stephen all the other magi are quite young.  For the two
tribunals since the covenant was founded he has voted all of their
sigils except that of Justin of Tremere. 

Libellus is less than 35 miles from the Schola.
