* Located on the River Vee, a tributary of the Mayene
* The only thermal springs in north west France
* The legend tells of an old horse, Rapide, released into the forest by his master, who returned made young. The master, Seigneur Hughes de Tessé, then became young himself by the same means.
* It is the home of a small court of faeries
