* Located near Denver
* Vis Source provides 4 pawns of Terram Vis per year
* 4 precious gems per year are also produced by the cave
* There is a witches kitchen in this location
* Eric, the Estate Manager, has opened up the main site and made it welcoming.
* Guest rooms are available and there's a great kitchen
* The aura is 2, rising to 4 in the cave
  * As of 1262 the aura is 4 in the regio, rising to 7 in the cave, and there is now an aura of 2 outside the regio
